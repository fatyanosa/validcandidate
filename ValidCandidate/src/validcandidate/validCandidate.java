/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validcandidate;

import java.util.ArrayList;
import java.util.List;
import org.testng.TestNG;

/**
 *
 * @author Tirana
 */
public class validCandidate {

    public static void main(String[] args) {
        
        // Create object of TestNG Class
        TestNG runner = new TestNG();

        // Create a list of String 
        List<String> suitefiles = new ArrayList<String>();

        // Add xml file which you have to execute
        suitefiles.add("D:\\Documents\\Coding\\We! Technology\\Bug Report\\ValidCandidate\\validcandidate\\ValidCandidate\\test\\TestPackage\\ValidCandidateTestSuite.xml");

        // now set xml file for execution
        runner.setTestSuites(suitefiles);

        // finally execute the runner using run method
        runner.run();
    }
}
