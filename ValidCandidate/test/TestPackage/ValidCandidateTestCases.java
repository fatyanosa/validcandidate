package TestPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import static org.testng.Assert.*;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

/**
 *
 * @author Tirana
 */
public class ValidCandidateTestCases {

    private static WebDriver driver = null;

    //Test Data
    String webDriver = "webdriver.chrome.driver"; //"webdriver.gecko.driver"
    String webDriverURL = "C:\\webdrivers\\chromedriver.exe"; //"C:\\webdrivers\\geckodriver.exe"
    private boolean acceptNextAlert = true;
    String validRecruiterUsername = "qamendacc.one@gmail.com";
    String validPassword = "password";
    String newPassword = "Passw0rd";
    String unregisteredEmail = "testing@gmail.com";
    String invalidPassword = "testing123";
    String invalidEmail = "testing";
    String unconfirmedAccount = "tirana.fatyanosa.wetechnology@gmail.com";
    String unconfirmedAccountPassword = "testing123";
    String newAccount = "tirana.test1@gmail.com";
    int index;
    int page = 1;

    String companyName = "Technology";
    String subdomainAddress = "wetechnology2";
    String websiteAddress = "http://wetechnology.cz/";
    String companyEmail = "tirana.fatyanosa@wetechnology.cz";
    String phoneNumber = "12345678";

    String companyNameNew = "Technology";
    String subdomainAddressNew = "we";
    String websiteAddressNew = "http://www.qarecruiterdev.com";

    String deleteCalendarAlert = "Do you want to delete the calendar?";
    String deleteTeamMemberAlert = "Do you want to delete the team member?";
    String uploadURL = "D:\\Documents\\Coding\\We! Technology\\Bug Report\\ValidCandidate\\Web\\AppElements.JPG";
    String fileName = "AppElements.JPG";
    String message = "Test send message to the applicant.";
    String calendarName = "Calendar 1";
    String newCalendarName = "New Calendar";
    String eventName = "Event Title";
    String newEventName = "New Event Title";

    //Recruiter in Prod
//    String url = "https://www.validcandidate.com/";
//    String createCompanyURL = "https://www.validcandidate.com/Dashboard/company/new";
//    String personalProfileURL = "https://www.validcandidate.com/dashboard/personal-profile";
//    String recruiterRegistrationURL = "https://www.validcandidate.com/account/recruiter/registration";
//    String recruiterDashboardURL = "https://www.validcandidate.com/dashboard";
//    String unconfirmedAccountURL = "https://www.validcandidate.com/Account/UnconfirmedAccount";
//    String candidatesPageURL = "https://www.validcandidate.com/Dashboard/Applications";
//    String calendarURL = "https://www.validcandidate.com/calendar";
//    String assessmentsURL = "https://www.validcandidate.com/assessment";
//    String ESDashboardURL = "https://www.validcandidate.com/es/dashboard";
//    String ENDashboardURL = "https://www.validcandidate.com/en/dashboard";
//    String ITDashboardURL = "https://www.validcandidate.com/it/dashboard";
//    String UKDashboardURL = "https://www.validcandidate.com/uk/dashboard";
//    String CSDashboardURL = "https://www.validcandidate.com/cs/dashboard";
//    String SRDashboardURL = "https://www.validcandidate.com/sr/dashboard";
    //Recruiter in dev
    String url = "http://recruiter.wetechnology.cz/";
    String createCompanyURL = "http://recruiter.wetechnology.cz/dashboard/company/new";
    String personalProfileURL = "http://recruiter.wetechnology.cz/dashboard/personal-profile";
    String recruiterRegistrationURL = "http://recruiter.wetechnology.cz/account/recruiter/registration";
    String recruiterDashboardURL = "http://recruiter.wetechnology.cz/dashboard";
    String unconfirmedAccountURL = "http://recruiter.wetechnology.cz/Account/UnconfirmedAccount";
    String candidatesPageURL = "http://recruiter.wetechnology.cz/Dashboard/Applications";
    String calendarURL = "http://recruiter.wetechnology.cz/calendar";
    String assessmentsURL = "http://recruiter.wetechnology.cz/assessment";
    String ESDashboardURL = "http://recruiter.wetechnology.cz/es/dashboard";
    String ENDashboardURL = "http://recruiter.wetechnology.cz/en/dashboard";
    String ITDashboardURL = "http://recruiter.wetechnology.cz/it/dashboard";
    String UKDashboardURL = "http://recruiter.wetechnology.cz/uk/dashboard";
    String CSDashboardURL = "http://recruiter.wetechnology.cz/cs/dashboard";
    String SRDashboardURL = "http://recruiter.wetechnology.cz/sr/dashboard";
    String jobTitle = "ASP.Net Frontend Developer";
    String jobDescription = "Are you excited about designing intuitive interfaces for customers who want to make sense of their data? Are you interested in building your career with a growing and collaborative team of designers? If so, we should talk!";
    String jobRequirements = "* The candidate must be fluent in English. \n"
            + "* Experience using testing frameworks and adoption of test driven development where applicable. \n"
            + "* Experience building ASP.Net MVC web applications \n"
            + "* Good knowledge of Javascript Frameworks (jQuery as a must) \n"
            + "* Orientation to Frontend programming (Javascript, HTML) \n"
            + "* Being obsessed with pixel perfect solutions ";
    String jobBenefits = "We are 100% remote team. That means we have a large amount of trust and a lot of flexibility.  \n"
            + "We offer a fixed monthly salary based on the applicants experience, skills and expertise, along with a monthly variable remuneration which is based on monthly performance. ";

    String validApplicantUsername = "qamendacc.two@gmail.com";//"tirana.applicant3@gmail.com";
    String unconfirmedApplicantAccount = "tirana.applicant4@gmail.com";
    String newApplicantAccount = "tirana.applicant4@gmail.com";
    String referenceEmail = "tirana.test2@gmail.com";
    String jobURL = "https://wetechnology2.validcandidate.com/jobs/6468/asp-net-frontend-developer";
    String appliedJobURL = "https://www.validcandidate.com/applicant/dashboard/job-application/8491";

    String hiredStatus = "Hired";
    String declinedStatus = "Declined Application";
    String cancelledStatus = "Application Cancelled";
    String updateProfileSuccess = "Successfully updated applicant information";

    //applicant in Prod
//    String applicantDashboardURL = "https://www.validcandidate.com/applicant/dashboard";
//    String applicantRegistrationURL = "https://www.validcandidate.com/account/applicant/registration";
//    String appliedJobsURL = "https://www.validcandidate.com/Applicant/Applications";
//    String referencesURL = "https://www.validcandidate.com/applicant/references";
//    String applicantAssessmentURL = "https://www.validcandidate.com/assessmentcandidate";
//    String applicantMessageURL = "https://www.validcandidate.com/messages";
//    String EditApplicantProfileURL = "https://www.validcandidate.com/applicant/edit-profile";
//    String ESApplicantDashboardURL = "https://www.validcandidate.com/es/applicant/dashboard";
//    String ITApplicantDashboardURL = "https://www.validcandidate.com/it/applicant/dashboard";
//    String UKApplicantDashboardURL = "https://www.validcandidate.com/uk/applicant/dashboard";
//    String CSApplicantDashboardURL = "https://www.validcandidate.com/cs/applicant/dashboard";
//    String SRApplicantDashboardURL = "https://www.validcandidate.com/sr/applicant/dashboard";
//    String ENApplicantDashboardURL = "https://www.validcandidate.com/en/applicant/dashboard";
    //applicant in Dev
    String applicantDashboardURL = "http://recruiter.wetechnology.cz/applicant/dashboard";
    String applicantRegistrationURL = "http://recruiter.wetechnology.cz/account/applicant/registration";
    String appliedJobsURL = "http://recruiter.wetechnology.cz/Applicant/Applications";
    String referencesURL = "http://recruiter.wetechnology.cz/applicant/references";
    String applicantAssessmentURL = "http://recruiter.wetechnology.cz/assessmentcandidate";
    String applicantMessageURL = "http://recruiter.wetechnology.cz/messages";
    String EditApplicantProfileURL = "http://recruiter.wetechnology.cz/applicant/edit-profile";
    String ESApplicantDashboardURL = "http://recruiter.wetechnology.cz/es/applicant/dashboard";
    String ITApplicantDashboardURL = "http://recruiter.wetechnology.cz/it/applicant/dashboard";
    String UKApplicantDashboardURL = "http://recruiter.wetechnology.cz/uk/applicant/dashboard";
    String CSApplicantDashboardURL = "http://recruiter.wetechnology.cz/cs/applicant/dashboard";
    String SRApplicantDashboardURL = "http://recruiter.wetechnology.cz/sr/applicant/dashboard";
    String ENApplicantDashboardURL = "http://recruiter.wetechnology.cz/en/applicant/dashboard";

    public ValidCandidateTestCases() {
    }

    @BeforeSuite(alwaysRun = true)
    public void setupBeforeSuite() {
        System.setProperty(webDriver, webDriverURL);
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
    }

    @AfterSuite(alwaysRun = true)
    public void setupAfterSuite() throws InterruptedException {
        Thread.sleep(10000);
        driver.quit();
    }

    @Test(description = "Launches the site")
    public void launchSite() throws Exception {
        assertEquals(driver.getTitle(), "Valid Candidate - Applicant Tracking System");
    }

    @Test(description = "Enters valid login data")
    public void validRecruiterLogin() throws Exception {
        driver.get(url);
        Thread.sleep(5000);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(validRecruiterUsername);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        assertEquals(driver.getCurrentUrl(), recruiterDashboardURL);
    }

    @Test(description = "Enter unregistered email")
    public void unregisteredEmail() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(unregisteredEmail);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        String actual_msg = driver.findElement(By.xpath("//div[contains(@class,'validation-summary-errors text-danger')]")).getText();
        String expect = "Invalid login attempt.";
        assertEquals(actual_msg, expect);
    }

    @Test(description = "Enter invalid password")
    public void invalidPassword() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(validRecruiterUsername);
        driver.findElement(By.name("Password")).sendKeys(invalidPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        String actual_msg = driver.findElement(By.xpath("//div[contains(@class,'validation-summary-errors text-danger')]")).getText();
        String expect = "Invalid login attempt.";
        assertEquals(actual_msg, expect);
    }

    @Test(description = "Enter invalid email")
    public void invalidEmail() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(invalidEmail);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        String actual_msg = driver.findElement(By.xpath("//span[contains(@class,'text-danger field-validation-error')]")).getText();
        String expect = "The Email field is not a valid e-mail address.";
        assertEquals(actual_msg, expect);
    }

    @Test(description = "Enter an uncorfirmed account")
    public void unconfirmedAccount() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(unconfirmedAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        assertEquals(driver.getTitle(), "Resend Email Confirmation - Valid Candidate");
        assertTrue(driver.getCurrentUrl().contains(unconfirmedAccountURL));
    }

    @Test(description = "Resend confirmation email")
    public void resendConfirmationEmail() throws Exception {
        driver.findElement(By.xpath("//button[contains(@class,'btn btn-ats ats-primary')]")).click();
        assertEquals(driver.getTitle(), "You've successfully registered - Valid Candidate");
    }

    @Test(description = "Confirmation email sent to the account's email")
    public void confirmationEmailSent() throws Exception {
        //Open gmail
        driver.get("http://www.gmail.com");

        // Enter userd id
        WebElement element = driver.findElement(By.id("identifierId"));
        element.sendKeys(unconfirmedAccount);

        //wait 5 secs for userid to be entered
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //driver.findElement(By.cssSelector("input[value='Next']")).click();
        //driver.findElement(By.xpath("//span[contains(@value,'Next')]")).click();
        driver.findElement(By.xpath("//span[contains(@class,'RveJvd snByac') and contains(text(), 'Next')]")).click();

        //Enter Password
        WebElement element1 = driver.findElement(By.name("password"));
        element1.sendKeys(unconfirmedAccountPassword);

        //wait 5 secs for userid to be entered
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//div[contains(@id,'passwordNext')]")).click();
        //driver.findElement(By.id("passwordNext")).click();

        driver.findElement(By.xpath("//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();

        List<WebElement> unreademeil = driver.findElements(By.xpath("//*[@class='zF']"));
    }

    @Test(description = "Login using LinkedIn")
    public void loginLinkedIn() throws Exception {
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test(description = "Sign Up as a Recruiter")
    public void signUpRecruiter() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As a Recruiter")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Recruiter Sign-up");
        assertTrue(driver.getCurrentUrl().contains(recruiterRegistrationURL));

        driver.findElement(By.name("Email")).sendKeys(unconfirmedAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.name("ConfirmPassword")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='checkbox'][id='is-term-condition-accepted']")).click();
        driver.findElement(By.cssSelector("input[type='submit'][value='Register']")).click();

        assertEquals(driver.getTitle(), "You've successfully registered - Valid Candidate");
    }

    @Test(description = "Sign Up as a Recruiter using LinkedIn")
    public void signUpRecruiterLinkedIn() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As a Recruiter")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Recruiter Sign-up");
        assertTrue(driver.getCurrentUrl().contains(recruiterRegistrationURL));

        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test(description = "Login using confirmed account (new account)")
    public void loginNewAccount() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(newAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();
        assertEquals(driver.getCurrentUrl(), personalProfileURL);
    }

    @Test(description = "Create a new company")
    public void createNewCompany() throws Exception {
        driver.findElement(By.cssSelector("span[class='avatar avatar-online']")).click();
        driver.findElement(By.linkText("Create a new company")).click();
        assertEquals(driver.getCurrentUrl(), createCompanyURL);

        driver.findElement(By.id("Company_CompanyName")).sendKeys(companyName);
        driver.findElement(By.id("Company_Subdomain")).sendKeys(subdomainAddress);
        driver.findElement(By.id("Company_WebsiteAddress")).sendKeys(websiteAddress);
        driver.findElement(By.id("Company_CompanyEmail")).sendKeys(companyEmail);
        driver.findElement(By.id("Company_PhoneNumber")).clear();
        driver.findElement(By.id("Company_PhoneNumber")).sendKeys(phoneNumber);
        driver.findElement(By.cssSelector("input[type='submit'][value='Save']")).click();
        String msg_in_alert = "Successfully created new company.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test(description = "Create an existing company")
    public void createExistCompany() throws Exception {
        driver.findElement(By.cssSelector("span[class='avatar avatar-online']")).click();
        driver.findElement(By.linkText("Create a new company")).click();
        assertEquals(driver.getCurrentUrl(), createCompanyURL);

        driver.findElement(By.id("Company_CompanyName")).sendKeys(companyName);
        driver.findElement(By.id("Company_Subdomain")).sendKeys(subdomainAddress);
        driver.findElement(By.id("Company_WebsiteAddress")).sendKeys(websiteAddress);
        driver.findElement(By.id("Company_CompanyEmail")).sendKeys(companyEmail);
        driver.findElement(By.cssSelector("input[type='submit'][value='Save']")).click();
        String msg_in_alert = "You already have a company with same name";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test(description = "Edit company")
    public void editCompany() throws Exception {
        driver.findElement(By.cssSelector("span[class='avatar avatar-online']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//a[@href='/dashboard/personal-profile']")).click();
        assertEquals(driver.getCurrentUrl(), personalProfileURL);

        driver.findElement(By.xpath("//a[@href='#company-profile']")).click();

        driver.findElement(By.id("CompanyVM_Company_CompanyName")).sendKeys(companyNameNew);
        driver.findElement(By.id("CompanyVM_Company_Subdomain")).sendKeys(subdomainAddressNew);
        driver.findElement(By.id("CompanyVM_Company_WebsiteAddress")).sendKeys(websiteAddressNew);
        driver.findElement(By.xpath("//button[contains(@class,'btn btn-block btn-primary pull-right waves-effect waves-classic') and contains(text(), 'Save')]")).click();

        String msg_in_alert = "Company successfully updated.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test
    public void testEditProfile() throws Exception {
        driver.findElement(By.cssSelector("span.avatar.avatar-online > i")).click();
        driver.findElement(By.linkText("Edit Profile")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), personalProfileURL);
        driver.findElement(By.id("FirstName")).clear();
        driver.findElement(By.id("FirstName")).sendKeys("QA");
        driver.findElement(By.id("LastName")).clear();
        driver.findElement(By.id("LastName")).sendKeys("Analyst");
        driver.findElement(By.id("JobTitle")).clear();
        driver.findElement(By.id("JobTitle")).sendKeys("QA Analyst");
        driver.findElement(By.id("autocomplete")).clear();
        driver.findElement(By.id("autocomplete")).sendKeys("hs");
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN, Keys.ENTER);
        driver.findElement(By.id("ContactNumber")).clear();
        driver.findElement(By.id("ContactNumber")).sendKeys("+6312345678");
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Successfully updated personal profile.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test(description = "Create a Job")
    public void createAJob() throws Exception {
        driver.findElement(By.xpath("//a[@href='/dashboard/job/Create']")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("JobTitle")).sendKeys(jobTitle);
        driver.findElement(By.name("Address")).sendKeys("a");
        Thread.sleep(3000);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN, Keys.ENTER);

        driver.findElement(By.id("JobDescription")).sendKeys(jobDescription);
        driver.findElement(By.id("Requirements")).sendKeys(jobRequirements);
        driver.findElement(By.id("Benefits")).sendKeys(jobBenefits);
        driver.findElement(By.id("btnNewJobPost")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Successfully created job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(5000);
        driver.findElement(By.id("btnFormNext")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("btnInterviewNext")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("btnHeaderFrmPublish")).click();
        Thread.sleep(2000);
        msg_in_alert = "Job successfully published";
        //assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test(description = "Create a Job")
    public void createAJobDev() throws Exception {
        Thread.sleep(5000);
        driver.findElement(By.xpath("//a[@href='/dashboard/job/Create']")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("JobTitle")).sendKeys(jobTitle);
        driver.findElement(By.name("Address")).sendKeys("a");
        Thread.sleep(3000);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN);
        driver.findElement(By.name("Address")).sendKeys(Keys.DOWN, Keys.ENTER);
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@id='page-content']/div[2]/div/div/div[4]/div/div/div/div[3]/div[2]")).sendKeys(jobDescription);

        driver.findElement(By.xpath("//div[@id='page-content']/div[2]/div/div/div[5]/div/div/div/div[3]/div[2]")).sendKeys(jobRequirements);

        driver.findElement(By.xpath("//div[@id='page-content']/div[2]/div/div/div[6]/div/div/div/div[3]/div[2]")).sendKeys(jobBenefits);
//        driver.findElement(By.id("JobDescription")).clear();
//        driver.findElement(By.id("JobDescription")).sendKeys(jobDescription);
//        driver.findElement(By.id("Requirements")).clear();
//        driver.findElement(By.id("Requirements")).sendKeys(jobRequirements);
//        driver.findElement(By.id("Benefits")).clear();
//        driver.findElement(By.id("Benefits")).sendKeys(jobBenefits);
        driver.findElement(By.id("btnNewJobPost")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Successfully created job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(5000);
        Thread.sleep(5000);
        driver.findElement(By.id("btnFormNext")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("btnInterviewNext")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("btnHeaderFrmPublish")).click();
        msg_in_alert = "Job successfully published";
    }

    @Test(description = "Edit a Job")
    public void editAJob() throws Exception {
        driver.findElement(By.cssSelector("i.icon.md-edit")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("JobDescription")).clear();
        driver.findElement(By.id("JobDescription")).sendKeys(jobDescription);
        driver.findElement(By.id("Requirements")).clear();
        driver.findElement(By.id("Requirements")).sendKeys(jobRequirements);
        driver.findElement(By.id("Benefits")).clear();
        driver.findElement(By.id("Benefits")).sendKeys(jobBenefits);
        driver.findElement(By.id("btnHeaderJobPostingSave")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Successfully updated job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test
    public void testOpenAJob() throws Exception {
        Thread.sleep(3000);
        String jobTitle = driver.findElement(By.xpath("//table[@id='jobContainer']/tbody/tr/td/a")).getText();
        driver.findElement(By.xpath("//table[@id='jobContainer']/tbody/tr/td/a")).click();
        Thread.sleep(3000);
        assertEquals(driver.findElement(By.xpath("//div[@id='heading-0']/div/div/a")).getText(), (jobTitle));
        jobURL = driver.getCurrentUrl();
    }

    @Test
    public void testDeleteJob() throws Exception {
        int catIndex = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='jobContainer']/tbody/tr"));

        for (WebElement element : allElements) {
            if (element.getText().contains(jobTitle)) {
                break;
            }
            catIndex += 1;
        }

        driver.findElement(By.xpath("//table[@id='jobContainer']/tbody/tr[" + (catIndex + 1) + "]/td[7]")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Job has been deleted.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test(description = "Invite a team member")
    public void testAddTeamMember() throws Exception {
        driver.findElement(By.cssSelector("i.icon.md-plus")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("EmailAddress")).clear();
        driver.findElement(By.id("EmailAddress")).sendKeys(newAccount);
        driver.findElement(By.id("send-invitation")).click();
        Thread.sleep(3000);
        String TeamMembers = driver.findElement(By.xpath("//*[contains(text(), newAccount)]")).getText();
        assertTrue(TeamMembers.contains(newAccount));
    }

    @Test
    public void testAddTeamMemberAsAdmin() throws Exception {
        driver.findElement(By.cssSelector("i.icon.md-plus")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("EmailAddress")).clear();
        driver.findElement(By.id("EmailAddress")).sendKeys(newAccount);
        driver.findElement(By.xpath("(//input[@id='IsAdmin'])[2]")).click();
        driver.findElement(By.id("send-invitation")).click();
        Thread.sleep(3000);
        String TeamMembers = driver.findElement(By.xpath("//*[contains(text(), newAccount)]")).getText();
        assertTrue(TeamMembers.contains(newAccount));
    }

    @Test
    public void testDeleteTeamMember() throws Exception {
        int catIndex = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//tbody[@id='tdMembers']/tr"));

        for (WebElement element : allElements) {
            if (element.getText().contains(newAccount)) {
                break;
            }
            catIndex += 1;
        }
        driver.findElement(By.xpath("(//button[@type='button'])[" + (catIndex + 1) + "]")).click();
        String deleteMemberPopup = driver.findElement(By.xpath("//*[contains(text(), deleteSubuserAlert)]")).getText();
        assertTrue(deleteMemberPopup.contains(deleteTeamMemberAlert));

        driver.findElement(By.cssSelector("button.btn.btn-danger")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Team member succesfully deleted";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));

        String TeamMembers = driver.findElement(By.xpath("//*[contains(text(), newAccount)]")).getText();
        assertFalse(TeamMembers.contains(newAccount));
    }

    @Test
    public void testChangePassword() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[10]/a/span/img")).click();
        driver.findElement(By.linkText("Settings and Subscription")).click();
        driver.findElement(By.linkText("Password")).click();
        driver.findElement(By.id("OldPassword")).clear();
        driver.findElement(By.id("OldPassword")).sendKeys(validPassword);
        driver.findElement(By.id("NewPassword")).clear();
        driver.findElement(By.id("NewPassword")).sendKeys(newPassword);
        driver.findElement(By.id("ConfirmPassword")).clear();
        driver.findElement(By.id("ConfirmPassword")).sendKeys(newPassword);
        driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Password Successfully changed..";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(5000);
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[10]/a/span/img")).click();
        driver.findElement(By.linkText("Log out")).click();

        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(validRecruiterUsername);
        driver.findElement(By.name("Password")).sendKeys(newPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();
        assertEquals(driver.getCurrentUrl(), recruiterDashboardURL);

        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[10]/a/span/img")).click();
        driver.findElement(By.linkText("Settings and Subscription")).click();
        driver.findElement(By.linkText("Password")).click();
        driver.findElement(By.id("OldPassword")).clear();
        driver.findElement(By.id("OldPassword")).sendKeys(newPassword);
        driver.findElement(By.id("NewPassword")).clear();
        driver.findElement(By.id("NewPassword")).sendKeys(validPassword);
        driver.findElement(By.id("ConfirmPassword")).clear();
        driver.findElement(By.id("ConfirmPassword")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
        Thread.sleep(3000);
        msg_in_alert = "Password Successfully changed..";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test
    public void testLogout() throws Exception {
        driver.get(recruiterDashboardURL);
        driver.findElement(By.cssSelector("span.avatar.avatar-online > i")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), url);
    }

    @Test
    public void testOfferJob() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int numApplied;
        page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div[2]/div[2]/button[2]")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");

        driver.findElement(By.id("clockpicker-input")).clear();
        driver.findElement(By.id("clockpicker-input")).sendKeys("18:30");

        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Skype");

        driver.findElement(By.id("establishFirstContactButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnConfirmEstablishFirstContact")).click();
        Thread.sleep(2000);
        msg_in_alert = "First contact details updated successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));

        driver.findElement(By.id("promoteCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");

        driver.findElement(By.id("clockpicker-input")).clear();
        driver.findElement(By.id("clockpicker-input")).sendKeys("18:30");
        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Skype");
        driver.findElement(By.id("establishInterviewDateButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnConfirmEstablishInterview")).click();
        Thread.sleep(2000);
        msg_in_alert = "Interview details updated successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("promoteCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("salary-offer-input")).clear();
        driver.findElement(By.id("salary-offer-input")).sendKeys("1000");
        driver.findElement(By.id("payment-terms-select")).click();
        driver.findElement(By.id("payment-terms-select")).sendKeys(Keys.DOWN, Keys.ENTER);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");
        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Please be on time.");
        driver.findElement(By.id("sendOfferButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Offer sent successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.navigate().refresh();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(5000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Offer"));
    }

    @Test
    public void testSkipSteps() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numApplied;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div[2]/div[2]/button[2]")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("skipFirstContactStepButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnConfirmSkipFirstContact")).click();
        Thread.sleep(2000);
        msg_in_alert = "First contact successfully skipped.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("promoteCandidateButton")).click();
        Thread.sleep(2000);
        Thread.sleep(2000);
        msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.findElement(By.id("skipInterviewStepButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnConfirmSkipInterview")).click();
        Thread.sleep(2000);
        msg_in_alert = "Interview successfully skipped.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("promoteCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("salary-offer-input")).clear();
        driver.findElement(By.id("salary-offer-input")).sendKeys("1000");
        new Select(driver.findElement(By.id("payment-terms-select"))).selectByVisibleText("Daily");
        driver.findElement(By.cssSelector("option[value=\"daily\"]")).click();
        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Please be on time.");
        driver.findElement(By.id("sendOfferButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Offer sent successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.navigate().refresh();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(2000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Offer"));
    }

    @Test
    public void testDeclineApplication() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numApplied;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div[2]/div[2]/button")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Application Declined.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.navigate().refresh();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(2000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Declined"));
    }

    @Test
    public void testDeclineFirstContact() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int numApplied;
        page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div[2]/div[2]/button[2]")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");

        driver.findElement(By.id("clockpicker-input")).clear();
        driver.findElement(By.id("clockpicker-input")).sendKeys("18:30");

        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Skype");

        driver.findElement(By.id("establishFirstContactButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnConfirmEstablishFirstContact")).click();
        Thread.sleep(2000);
        msg_in_alert = "First contact details updated successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("declineCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Application Declined.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.navigate().refresh();
        Thread.sleep(5000);
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,-250)", "");
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(2000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Declined"));
    }

    @Test
    public void testDeclineInterview() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numApplied;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div[2]/div[2]/button[2]")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");

        driver.findElement(By.id("clockpicker-input")).clear();
        driver.findElement(By.id("clockpicker-input")).sendKeys("18:30");

        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Skype");

        driver.findElement(By.id("establishFirstContactButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnConfirmEstablishFirstContact")).click();
        Thread.sleep(2000);
        msg_in_alert = "First contact details updated successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("promoteCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");

        driver.findElement(By.id("clockpicker-input")).clear();
        driver.findElement(By.id("clockpicker-input")).sendKeys("18:30");
        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Skype");
        driver.findElement(By.id("establishInterviewDateButton")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("btnConfirmEstablishInterview")).click();
        Thread.sleep(2000);
        msg_in_alert = "Interview details updated successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("declineCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Application Declined.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.navigate().refresh();
        Thread.sleep(2000);
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(2000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Declined"));
    }

    @Test
    public void testDeclineOffer() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numApplied;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div[2]/div[2]/button[2]")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");

        driver.findElement(By.id("clockpicker-input")).clear();
        driver.findElement(By.id("clockpicker-input")).sendKeys("18:30");

        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Skype");

        driver.findElement(By.id("establishFirstContactButton")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnConfirmEstablishFirstContact")).click();
        Thread.sleep(2000);
        msg_in_alert = "First contact details updated successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("promoteCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("datepicker-input")).clear();
        driver.findElement(By.id("datepicker-input")).sendKeys("11/10/17");

        driver.findElement(By.id("clockpicker-input")).clear();
        driver.findElement(By.id("clockpicker-input")).sendKeys("18:30");
        driver.findElement(By.id("other-details-textarea")).clear();
        driver.findElement(By.id("other-details-textarea")).sendKeys("Skype");
        driver.findElement(By.id("establishInterviewDateButton")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("btnConfirmEstablishInterview")).click();
        Thread.sleep(2000);
        msg_in_alert = "Interview details updated successfully.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("promoteCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Successfully promoted job.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        driver.findElement(By.id("declineCandidateButton")).click();
        Thread.sleep(2000);
        msg_in_alert = "Application Declined.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.navigate().refresh();
        Thread.sleep(2000);
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(2000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Declined"));
    }

    @Test
    public void testAddNewItemScoreCard() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numApplied;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("toolbar-button")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("a.tool-item > i.icon.wb-star")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("newItem")).click();
        driver.findElement(By.cssSelector("#editableTable > tbody > tr > td")).click();
        driver.findElement(By.cssSelector("div.row.slidePanel-details > input")).clear();
        driver.findElement(By.cssSelector("div.row.slidePanel-details > input")).sendKeys("Item 1");
        driver.findElement(By.cssSelector("img[alt=\"5\"]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("newItem")).click();
        driver.findElement(By.xpath("//table[@id='editableTable']/tbody/tr[2]/td")).click();
        driver.findElement(By.cssSelector("div.row.slidePanel-details > input")).clear();
        driver.findElement(By.cssSelector("div.row.slidePanel-details > input")).sendKeys("Item 2");
        driver.findElement(By.xpath("(//img[@alt='4'])[2]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("newItem")).click();
        driver.findElement(By.xpath("//table[@id='editableTable']/tbody/tr[3]/td")).click();
        driver.findElement(By.cssSelector("div.row.slidePanel-details > input")).clear();
        driver.findElement(By.cssSelector("div.row.slidePanel-details > input")).sendKeys("Item 3");
        driver.findElement(By.xpath("(//img[@alt='3'])[3]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div/div[2]/button[2]")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Scorecard successfully updated";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(5000);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(1);
        String score = df.format(Double.parseDouble(driver.findElement(By.xpath("//div[@id='rating-total']/input")).getAttribute("value")));
        assertTrue(driver.findElement(By.xpath("//a[@id='slidePanelHeaderScore']")).getText().contains(score));
        driver.navigate().refresh();
        Thread.sleep(2000);
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(2000);
        String candidateDetails = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateDetails.contains(score));
    }

    @Test
    public void testDeleteItemScoreCard() throws Exception {
        driver.navigate().refresh();
        int index;
        int numApplied;
        int page = 1;
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("toolbar-button")).click();
        driver.findElement(By.cssSelector("a.tool-item > i.icon.wb-star")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText("X")).click();
        assertTrue(closeAlertAndGetItsText().matches("^Are you sure you want to remove this item[\\s\\S]$"));
        Thread.sleep(2000);
        String msg_in_alert = "Scorecard successfully updated";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.findElement(By.linkText("X")).click();
        assertTrue(closeAlertAndGetItsText().matches("^Are you sure you want to remove this item[\\s\\S]$"));
        Thread.sleep(2000);
        msg_in_alert = "Scorecard successfully updated";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.findElement(By.linkText("X")).click();
        assertTrue(closeAlertAndGetItsText().matches("^Are you sure you want to remove this item[\\s\\S]$"));
        Thread.sleep(2000);
        msg_in_alert = "Scorecard successfully updated";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        //driver.findElement(By.xpath("//button[@onclick='saveScore(8482)']")).click();
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div/div[2]/button[2]")).click();
        Thread.sleep(2000);
        msg_in_alert = "Scorecard could not be saved.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));

        driver.navigate().refresh();
        Thread.sleep(2000);
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(2000);
        String candidateDetails = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateDetails.contains("0.0"));
        Thread.sleep(2000);
        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(2000);
        assertTrue(driver.findElement(By.xpath("//a[@id='slidePanelHeaderScore']")).getText().contains("0,0"));
    }

    @Test
    public void testAddAttachments() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numApplied;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("toolbar-button")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("a.tool-item > i.icon.wb-star")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("attachment")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div/table[2]/tfoot/tr/th/form/label")).click();
        Thread.sleep(2000);
        StringSelection ss = new StringSelection(uploadURL);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        Robot robot = new Robot();
        robot.delay(5000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(5000);
        robot.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(10000);
        String attachments = driver.findElement(By.xpath("//*[contains(text(), fileName)]")).getText();
        assertTrue(attachments.contains(fileName));
    }

    @Test
    public void testDownloadAttachment() throws Exception {
        driver.findElement(By.linkText("Download")).click();
    }

    @Test
    public void testDeleteAttachment() throws Exception {
        Thread.sleep(2000);
        driver.findElement(By.linkText("X")).click();
        assertTrue(closeAlertAndGetItsText().matches("^Are you sure you want to remove this item[\\s\\S]$"));
        Thread.sleep(2000);
        String msg_in_alert = "Attachments successfully updated.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
    }

    @Test
    public void testSendMessage() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numApplied;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        do {
            index = 0;
            numApplied = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numApplied += 1;
                    break;
                }
                index += 1;
            }
            if (numApplied == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numApplied == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("toolbar-button")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/header/div[2]/div/a[2]/i")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Message")).clear();
        driver.findElement(By.id("Message")).sendKeys(message);
        driver.findElement(By.id("fileupload")).click();
        StringSelection ss = new StringSelection(uploadURL);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        Robot robot = new Robot();
        robot.delay(5000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(5000);
        robot.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(5000);

        driver.findElement(By.id("btnSendMessage")).click();
        Thread.sleep(2000);
        String messages = driver.findElement(By.xpath("//*[contains(text(), message)]")).getText();
        assertTrue(messages.contains(message));
    }

    @Test
    public void testCreateNewCalendar() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[5]/a/i/img")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), calendarURL);
        driver.findElement(By.xpath("(//button[@type='button'])[15]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("txtNewCalendarName")).clear();
        driver.findElement(By.id("txtNewCalendarName")).sendKeys(calendarName);
        driver.findElement(By.id("btnNewCalendarSave")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Calendar successfully saved.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        assertEquals(driver.findElement(By.xpath("//div[@id='divCalendarList']/div/div/span")).getText(), calendarName);
    }

    @Test
    public void testEditCalendar() throws Exception {
        int catIndex = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='divCalendarList']/div"));

        for (WebElement element : allElements) {
            if (element.getText().contains(calendarName)) {
                break;
            }
            catIndex += 1;
        }

        driver.findElement(By.xpath("//div[@id='divCalendarList']/div[" + (catIndex + 1) + "]/div/span")).click();
        driver.findElement(By.xpath("//div[@id='divCalendarList']/div[" + (catIndex + 1) + "]/div/div/span/i")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Calendar_" + catIndex + "__Name")).clear();
        driver.findElement(By.id("Calendar_" + catIndex + "__Name")).sendKeys(newCalendarName);
        driver.findElement(By.id("Calendar_" + catIndex + "__Name")).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        String msg_in_alert = "Calendar succesfully updated.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        assertEquals(driver.findElement(By.xpath("//div[@id='divCalendarList']/div[" + (catIndex + 1) + "]/div/span")).getText(), newCalendarName);
    }

    @Test
    public void testDeleteCalendar() throws Exception {
        int catIndex = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='divCalendarList']/div"));

        for (WebElement element : allElements) {
            if (element.getText().contains(newCalendarName)) {
                break;
            }
            catIndex += 1;
        }

        driver.findElement(By.xpath("//div[@id='divCalendarList']/div[" + (catIndex + 1) + "]/div/span")).click();
        driver.findElement(By.xpath("//div[@id='divCalendarList']/div[" + (catIndex + 1) + "]/div/div/span[2]/i")).click();
        Thread.sleep(2000);
        String alert = driver.findElement(By.xpath("//*[contains(text(), deleteCalendarAlert)]")).getText();
        assertTrue(alert.contains(deleteCalendarAlert));
        driver.findElement(By.cssSelector("div.modal-footer > button.btn.btn-danger")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Calendar succesfully deleted";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        Thread.sleep(2000);
        String calendarNames = driver.findElement(By.xpath("//*[contains(text(), newCalendarName)]")).getText();
        assertFalse(calendarNames.contains(newCalendarName));
    }

    @Test
    public void testCreateNewEvent() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[5]/a/i/img")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), calendarURL);
        driver.findElement(By.id("addNewEventBtn")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("ename")).clear();
        driver.findElement(By.id("ename")).sendKeys(eventName);
        driver.findElement(By.id("newstarts")).clear();
        driver.findElement(By.id("newstarts")).sendKeys("October 11 ,2017");
        driver.findElement(By.id("newstarts")).sendKeys(Keys.TAB);
        driver.findElement(By.id("timeStart")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("timeStart")).sendKeys(Keys.TAB);
        driver.findElement(By.id("timeStart")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("timeStart")).sendKeys(Keys.TAB);
        driver.findElement(By.id("timeStart")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("newends")).clear();
        driver.findElement(By.id("newends")).sendKeys("October 12 ,2017");
        driver.findElement(By.id("newends")).sendKeys(Keys.TAB);
        driver.findElement(By.id("timeEnd")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("timeEnd")).sendKeys(Keys.TAB);
        driver.findElement(By.id("timeEnd")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("timeEnd")).sendKeys(Keys.TAB);
        driver.findElement(By.id("timeEnd")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("repeats")).clear();
        driver.findElement(By.id("repeats")).sendKeys("1");
        driver.findElement(By.id("eventColorChosen3")).click();
        driver.findElement(By.id("btnCreateEvent")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Event successfully saved.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));

        String eventNames = driver.findElement(By.xpath("//*[contains(text(), eventName)]")).getText();
        assertTrue(eventNames.contains(eventName));
    }

    @Test
    public void testEditEvent() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[5]/a/i/img")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), calendarURL);
        int catIndex = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='eventList']/a"));

        for (WebElement element : allElements) {
            if (element.getText().contains(eventName)) {
                break;
            }
            catIndex += 1;
        }
        driver.findElement(By.xpath("//div[@id='eventList']/a[" + (catIndex + 1) + "]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("editEname")).clear();
        driver.findElement(By.id("editEname")).sendKeys(newEventName);
        driver.findElement(By.id("editStarts")).clear();
        driver.findElement(By.id("editStarts")).sendKeys("October 13 ,2017");
        driver.findElement(By.id("editStarts")).sendKeys(Keys.TAB);
        driver.findElement(By.id("editTimeStart")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("editTimeStart")).sendKeys(Keys.TAB);
        driver.findElement(By.id("editTimeStart")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("editTimeStart")).sendKeys(Keys.TAB);
        driver.findElement(By.id("editTimeStart")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("editEnds")).clear();
        driver.findElement(By.id("editEnds")).sendKeys("October 14 ,2017");
        driver.findElement(By.id("editEnds")).sendKeys(Keys.TAB);
        driver.findElement(By.id("editTimeEnd")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("editTimeEnd")).sendKeys(Keys.TAB);
        driver.findElement(By.id("editTimeEnd")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("editTimeEnd")).sendKeys(Keys.TAB);
        driver.findElement(By.id("editTimeEnd")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("editRepeats")).clear();
        driver.findElement(By.id("editRepeats")).sendKeys("2");
        driver.findElement(By.id("editColorChosen4")).click();
        driver.findElement(By.id("btnEditEvent")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Event successfully updated.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        String eventNames = driver.findElement(By.xpath("//*[contains(text(), newEventName)]")).getText();
        assertTrue(eventNames.contains(newEventName));
    }

    @Test
    public void testDeleteEvent() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[5]/a/i/img")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), calendarURL);
        int catIndex = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='eventList']/a"));

        for (WebElement element : allElements) {
            if (element.getText().contains(newEventName)) {
                break;
            }
            catIndex += 1;
        }
        driver.findElement(By.xpath("//div[@id='eventList']/a[" + (catIndex + 1) + "]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("btnDeleteEvent")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("div.modal-footer > button.btn.btn-danger")).click();
        Thread.sleep(2000);
        String msg_in_alert = "Event succesfully deleted.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        String eventNames = driver.findElement(By.xpath("//*[contains(text(), newEventName)]")).getText();
        assertFalse(eventNames.contains(newEventName));
    }

    @Test
    public void testClickCalendar() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[5]/a/i/img")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), calendarURL);

        Date today = new Date();
        String dateOut;
        DateFormat dateFormatter;
        Calendar cal = Calendar.getInstance();

        int row = 1;
        List<WebElement> allWeeks = driver.findElements(By.xpath("//div[@id='calendar']/div[2]/div/table/tbody/tr/td/div/div/div"));
        List<String> dataDates = new ArrayList<String>();
        for (WebElement week : allWeeks) {
            List<WebElement> allDays = driver.findElements(By.xpath("//div[@id='calendar']/div[2]/div/table/tbody/tr/td/div/div/div[" + row + "]/div/table/tbody/tr/td"));
            for (WebElement day : allDays) {
                if (day.getAttribute("class").contains("today")) {
                    for (WebElement day2 : allDays) {
                        if (day2.getAttribute("data-date") != null) {
                            dataDates.add(day2.getAttribute("data-date"));
                        }
                    }
                    break;
                }
            }
            row += 1;
        }

        //Next Month button
        driver.findElement(By.xpath("(//button[@type='button'])[8]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[8]")).click();

        //Previous Month button
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();

        //Day button
        driver.findElement(By.xpath("(//button[@type='button'])[6]")).click();
        driver.findElement(By.xpath("//div[@id='calendar']/div/div/button")).click();

        dateFormatter = DateFormat.getDateInstance(DateFormat.LONG);
        today = new Date();
        dateOut = dateFormatter.format(today);

        assertEquals(driver.findElement(By.xpath("//div[@id='calendar']/div/div[3]/div/h2")).getText(), (dateOut));

        //Week button
        driver.findElement(By.xpath("(//button[@type='button'])[5]")).click();

        dateFormatter = new SimpleDateFormat("yy-MM-dd");
        Date date = dateFormatter.parse(dataDates.get(0));
        cal.setTime(date);
        int firstDay = cal.get(Calendar.DAY_OF_MONTH);

        date = dateFormatter.parse(dataDates.get(dataDates.size() - 1));
        cal.setTime(date);
        int lastDay = cal.get(Calendar.DAY_OF_MONTH);
        int year = cal.get(Calendar.YEAR);

        String month = Month.of(cal.get(Calendar.MONTH) + 1).name().toLowerCase();
        dateOut = (Character.toUpperCase(month.charAt(0)) + month.substring(1, 3)) + " " + firstDay + " – " + lastDay + ", " + year;

        assertEquals(driver.findElement(By.xpath("//div[@id='calendar']/div/div[3]/div/h2")).getText(), (dateOut));

        //Month button
        driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
        cal.setTime(today);
        SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy");
        dateOut = formatter.format(cal.getTime());
        assertEquals(driver.findElement(By.xpath("//div[@id='calendar']/div/div[3]/div/h2")).getText(), (dateOut));
    }

    @Test
    public void testAskForMoreAssessments() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        int index;
        int numAssessment;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        do {
            index = 0;
            numAssessment = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numAssessment += 1;
                    break;
                }
                index += 1;
            }
            if (numAssessment == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numAssessment == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(5000);

        String applicantName = driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/header/div/div[2]/h3")).getText().toLowerCase();

        driver.findElement(By.xpath("//div[@id='sliderPanel-wrapper']/div/div[2]/table/tbody/tr[12]/td[2]/a")).click();
        Thread.sleep(2000);

        for (int i = 0; i < 31; i++) {
            driver.findElement(By.xpath("//body")).sendKeys(Keys.TAB);
        }

        Robot robot = new Robot();
        robot.delay(5000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        double remainBalanceBefore = Double.parseDouble(driver.findElement(By.xpath("//div[@id='candidateAssessmentModal']/div/div/div[3]/div/div[2]/p/span")).getText());
        driver.findElement(By.xpath("//table[@id='dtAssessment']/tbody/tr")).click();
        String assessmentName = driver.findElement(By.xpath("//table[@id='dtAssessment']/tbody/tr/td")).getText().toLowerCase();
        double assessmentPrice = Double.parseDouble(driver.findElement(By.xpath("//table[@id='dtAssessment']/tbody/tr/td[3]/span")).getText());
        double remainBalanceAfter = Double.parseDouble(driver.findElement(By.xpath("//div[@id='candidateAssessmentModal']/div/div/div[3]/div/div[2]/p/span")).getText());
        assertEquals(remainBalanceBefore - assessmentPrice, remainBalanceAfter);
        driver.findElement(By.id("btnGetAssessment")).click();
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,-250)", "");
        driver.findElement(By.xpath("//a[@href='/Dashboard/Applications']")).click();
        Thread.sleep(2000);
        assertEquals(driver.findElement(By.xpath("//span[@id='lnkUserBalance']")).getText(), String.valueOf(remainBalanceAfter + "0"));

        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[4]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), assessmentsURL);

        page = 1;
        do {
            index = 0;
            numAssessment = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='dtAssessmentHistory']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().toLowerCase().contains(applicantName) && element.getText().toLowerCase().contains(assessmentName)) {
                    numAssessment += 1;
                    break;
                }
                index += 1;
            }
            if (numAssessment == 0) {
                driver.findElement(By.xpath("//li[@id='dtAssessmentHistory_next']/a")).click();
                page += 1;
            }
        } while (numAssessment == 0);
        assertEquals(driver.findElement(By.xpath("//table[@id='dtAssessmentHistory']/tbody/tr[" + (index + 1) + "]/td/a")).getText().toLowerCase(), applicantName);
        assertEquals(driver.findElement(By.xpath("//table[@id='dtAssessmentHistory']/tbody/tr[" + (index + 1) + "]/td[2]/span")).getText().toLowerCase(), assessmentName);
        assertEquals(driver.findElement(By.xpath("//table[@id='dtAssessmentHistory']/tbody/tr[" + (index + 1) + "]/td[5]")).getText().toLowerCase(), "pending");
    }

    @Test
    public void testSendMessageToApplicant() throws Exception {
        driver.findElement(By.linkText("Candidates")).click();
        Thread.sleep(2000);
        int index;
        int numAssessment;
        int page = 1;
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        do {
            index = 0;
            numAssessment = 0;
            List<WebElement> allElements = driver.findElements(By.xpath("//table[@id='candidates-table']/tbody/tr"));
            for (WebElement element : allElements) {
                if (element.getText().contains("Applied")) {
                    numAssessment += 1;
                    break;
                }
                index += 1;
            }
            if (numAssessment == 0) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
                page += 1;
            }
        } while (numAssessment == 0);

        driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).click();
        Thread.sleep(2000);

        driver.findElement(By.id("toolbar-button")).click();
        driver.findElement(By.cssSelector("a.tool-item > i.icon.wb-envelope")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("Message")).clear();
        driver.findElement(By.id("Message")).sendKeys(message);
        driver.findElement(By.id("fileupload")).clear();
        driver.findElement(By.id("fileupload")).sendKeys("D:\\Documents\\Coding\\We! Technology\\Bug Report\\ValidCandidate\\Web\\icon.jpg");
        driver.findElement(By.id("btnSendMessage")).click();
    }

    @Test
    public void testChangeLanguage() throws Exception {
        driver.findElement(By.cssSelector("span.flag-icon.flag-icon-us")).click();
        driver.findElement(By.linkText("Español")).click();
        assertEquals(driver.getCurrentUrl(), ESDashboardURL);
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[8]/a")).click();
        driver.findElement(By.linkText("Italiano")).click();
        assertEquals(driver.getCurrentUrl(), ITDashboardURL);
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[8]/a")).click();
        driver.findElement(By.linkText("Українська")).click();
        assertEquals(driver.getCurrentUrl(), UKDashboardURL);
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[8]/a")).click();
        driver.findElement(By.linkText("Český")).click();
        assertEquals(driver.getCurrentUrl(), CSDashboardURL);
        driver.findElement(By.cssSelector("span.flag-icon.flag-icon-cz")).click();
        driver.findElement(By.linkText("Српски")).click();
        assertEquals(driver.getCurrentUrl(), SRDashboardURL);
        driver.findElement(By.cssSelector("span.flag-icon.flag-icon-sr")).click();
        driver.findElement(By.linkText("English")).click();
        assertEquals(driver.getCurrentUrl(), ENDashboardURL);
    }

    @Test(description = "Login using a valid applicant login")
    public void validApplicantLogin() throws Exception {
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(validApplicantUsername);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        assertEquals(driver.getCurrentUrl(), applicantDashboardURL);
    }

    @Test(description = "Sign Up as an applicant")
    public void signUpApplicant() throws Exception {
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As an Applicant")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Applicant Sign-up");
        assertTrue(driver.getCurrentUrl().contains(applicantRegistrationURL));

        driver.findElement(By.name("Email")).sendKeys(unconfirmedApplicantAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.name("ConfirmPassword")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='checkbox'][id='is-term-condition-accepted']")).click();
        driver.findElement(By.cssSelector("input[type='submit'][value='Register']")).click();

        assertEquals(driver.getTitle(), "You've successfully registered - Valid Candidate");
    }

    @Test(description = "Login using LinkedIn")
    public void loginApplicantLinkedIn() throws Exception {
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test(description = "Sign Up as an Applicant using LinkedIn")
    public void signUpApplicantLinkedIn() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As an Applicant")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Applicant Sign-up");
        assertTrue(driver.getCurrentUrl().contains(applicantRegistrationURL));

        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test
    public void testApplyAJob() throws Exception {
        Thread.sleep(2000);
        int jobCountBefore = Integer.parseInt(driver.findElement(By.cssSelector("div[id='open-job-count']")).getText());
        driver.get(jobURL);
        driver.findElement(By.linkText("Apply Now")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("ApplicationQuestionAnswer_0__Answer")).clear();
        driver.findElement(By.id("ApplicationQuestionAnswer_0__Answer")).sendKeys("Test");
        driver.findElement(By.id("ApplicationQuestionAnswer_1__Answer")).clear();
        driver.findElement(By.id("ApplicationQuestionAnswer_1__Answer")).sendKeys("Test");
        driver.findElement(By.linkText("Video Recording")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("div.modal-header > button.close")).click();
        Thread.sleep(2000);
        new Select(driver.findElement(By.id("Availability"))).selectByVisibleText("ASAP");
        driver.findElement(By.id("CoverLetter")).clear();
        driver.findElement(By.id("CoverLetter")).sendKeys("Test");
        driver.findElement(By.id("add-application-reference")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("ins.iCheck-helper")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(5000);
        int jobCountAfter = Integer.parseInt(driver.findElement(By.cssSelector("div[id='open-job-count']")).getText());
        assertEquals(jobCountBefore + 1, jobCountAfter);
        driver.findElement(By.xpath("//div[@id='page-content']/div[4]/div[2]/a")).click();
        Thread.sleep(5000);
        appliedJobURL = driver.getCurrentUrl();
    }

    @Test(description = "Opening Applied Jobs page")
    public void openAppliedJobs() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[2]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), appliedJobsURL);
    }

    @Test
    public void testAcceptOffer() throws Exception {
        driver.get(appliedJobURL);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//li[4]/div")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        Thread.sleep(2000);
        String text = driver.findElement(By.xpath("//*[contains(text(), hiredStatus)]")).getText();
        assertTrue(text.contains(hiredStatus));
    }

    @Test
    public void testApplicantDeclineOffer() throws Exception {
        driver.get(appliedJobURL);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//li[4]/div")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
        Thread.sleep(5000);
        String text = driver.findElement(By.xpath("//*[contains(text(), declinedStatus)]")).getText();
        assertTrue(text.contains(declinedStatus));
    }

    @Test
    public void testCancelApplication() throws Exception {
        driver.findElement(By.xpath("//a[@href='/Applicant/Applications?selectedApplicationStatus=applied']")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText("View Details")).click();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(5000);
        String text = driver.findElement(By.xpath("//*[contains(text(), cancelledStatus)]")).getText();
        assertTrue(text.contains(cancelledStatus));
    }

    @Test
    public void testRequestNewReference() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[3]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), referencesURL);
        driver.findElement(By.id("new-ref")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("ReferenceName")).clear();
        driver.findElement(By.id("ReferenceName")).sendKeys("Tirana Fatyanosa");
        driver.findElement(By.id("ReferenceEmail")).clear();
        driver.findElement(By.id("ReferenceEmail")).sendKeys(referenceEmail);
        driver.findElement(By.cssSelector("div.selected-flag")).click();
        driver.findElement(By.cssSelector("span.country-name")).click();
        driver.findElement(By.id("PhoneNumber")).clear();
        driver.findElement(By.id("PhoneNumber")).sendKeys("+11234567890");
        driver.findElement(By.id("Relationship")).clear();
        driver.findElement(By.id("Relationship")).sendKeys("Peer");
        driver.findElement(By.id("ContactAverage")).clear();
        driver.findElement(By.id("ContactAverage")).sendKeys("Sometimes");
        driver.findElement(By.id("JobPosition")).clear();
        driver.findElement(By.id("JobPosition")).sendKeys("QA Analyst");
        driver.findElement(By.id("Company")).clear();
        driver.findElement(By.id("Company")).sendKeys("We Technology");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//form[@id='frm-reference']/div/div[10]/button")).click();
        Thread.sleep(10000);
        String references = driver.findElement(By.xpath("//*[contains(text(), referenceEmail)]")).getText();
        assertTrue(references.contains(referenceEmail));
    }

    @Test
    public void testEditReference() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[3]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), referencesURL);
        int index = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div"));

        for (WebElement element : allElements) {
            if (element.getText().contains(referenceEmail)) {
                break;
            }
            index += 1;
        }

        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div[" + (index + 1) + "]/div[3]/button")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("ReferenceName")).clear();
        driver.findElement(By.id("ReferenceName")).sendKeys("Tirana Fatyanosa");
        driver.findElement(By.id("ReferenceEmail")).clear();
        driver.findElement(By.id("ReferenceEmail")).sendKeys(unconfirmedApplicantAccount);
        driver.findElement(By.cssSelector("div.selected-flag")).click();
        driver.findElement(By.cssSelector("span.country-name")).click();
        driver.findElement(By.id("PhoneNumber")).clear();
        driver.findElement(By.id("PhoneNumber")).sendKeys("+11234567890");
        driver.findElement(By.id("Relationship")).clear();
        driver.findElement(By.id("Relationship")).sendKeys("Peer");
        driver.findElement(By.id("ContactAverage")).clear();
        driver.findElement(By.id("ContactAverage")).sendKeys("Sometimes");
        driver.findElement(By.id("JobPosition")).clear();
        driver.findElement(By.id("JobPosition")).sendKeys("QA Analyst");
        driver.findElement(By.id("Company")).clear();
        driver.findElement(By.id("Company")).sendKeys("We Technology");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//form[@id='frm-reference']/div/div[10]/button")).click();
        Thread.sleep(10000);
        String references = driver.findElement(By.xpath("//*[contains(text(), unconfirmedAccount)]")).getText();
        assertTrue(references.contains(unconfirmedApplicantAccount));
    }

    @Test
    public void testDeleteReference() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[3]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), referencesURL);
        int index = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div"));

        for (WebElement element : allElements) {
            if (element.getText().contains(unconfirmedApplicantAccount)) {
                break;
            }
            index += 1;
        }

        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div[" + (index + 1) + "]/div[3]/button[2]")).click();
        Thread.sleep(5000);
        assertTrue(closeAlertAndGetItsText().matches("^Are you sure you want to delete this reference[\\s\\S]$"));
        Thread.sleep(5000);
        String references = driver.findElement(By.xpath("//*[contains(text(), unconfirmedAccount)]")).getText();
        assertFalse(references.contains(unconfirmedApplicantAccount));
    }

    @Test
    public void testGetAssessment() throws Exception {
        driver.findElement(By.cssSelector("a.toolbox-ico > img")).click();
        Thread.sleep(5000);
        assertEquals(driver.getCurrentUrl(), applicantAssessmentURL);
        String assessmentName = driver.findElement(By.xpath("//tbody[@id='partialDatalist']/tr/td")).getText();
        driver.findElement(By.xpath("//tbody[@id='partialDatalist']/tr/td")).click();
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("assessmentButtonBuyNow")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Successfully sent assessment request.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        assertNotEquals(driver.findElement(By.xpath("//tbody[@id='partialDatalist']/tr/td")).getText(), assessmentName);

        driver.findElement(By.linkText("Assessment History")).click();
        Thread.sleep(5000);

        assertEquals(driver.findElement(By.xpath("//tbody[@id='modalAssessmentContainerHistory']/tr/td")).getText(), assessmentName.toUpperCase());
        assertEquals(driver.findElement(By.xpath("//tbody[@id='modalAssessmentContainerHistory']/tr/td[4]")).getText(), "PENDING");
    }

    @Test
    public void testSendMessageToRecruiter() throws Exception {
        String messageText = "Send a message.";
        driver.findElement(By.linkText("Details")).click();
        driver.findElement(By.linkText("Message")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("Message")).clear();
        driver.findElement(By.id("Message")).sendKeys(messageText);
        driver.findElement(By.cssSelector("button.browse.mf-browse-btn")).click();
        StringSelection ss = new StringSelection(uploadURL);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        Robot robot = new Robot();
        robot.delay(5000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(5000);
        robot.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(5000);
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Message sent successfully";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[5]/a/img")).click();
        Thread.sleep(3000);
        assertEquals(driver.getCurrentUrl(), applicantMessageURL);
        int index = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//ul[@id='listOfDialogs']/li"));

        for (WebElement element : allElements) {
            driver.findElement(By.xpath("//ul[@id='listOfDialogs']/li")).click();
            String messages = driver.findElement(By.xpath("//*[contains(text(), messageText)]")).getText();
            if (messages.contains(messageText)) {
                assertTrue(messages.contains(messageText));
                break;
            }
        }
    }

    @Test
    public void testChangeLanguageApplicant() throws Exception {
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Español")).click();
        assertEquals(driver.getCurrentUrl(), ESApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Italiano")).click();
        assertEquals(driver.getCurrentUrl(), ITApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Українська")).click();
        assertEquals(driver.getCurrentUrl(), UKApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Český")).click();
        assertEquals(driver.getCurrentUrl(), CSApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Српски")).click();
        assertEquals(driver.getCurrentUrl(), SRApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("English")).click();
        assertEquals(driver.getCurrentUrl(), ENApplicantDashboardURL);
    }

    @Test
    public void testEditProfileApplicant() throws Exception {
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Edit Profile")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), EditApplicantProfileURL);
        driver.findElement(By.id("FirstName")).clear();
        driver.findElement(By.id("FirstName")).sendKeys("QAApplicant One");
        driver.findElement(By.id("LastName")).clear();
        driver.findElement(By.id("LastName")).sendKeys("Endoma Jr");
        driver.findElement(By.id("JobTitle")).clear();
        driver.findElement(By.id("JobTitle")).sendKeys("QA Tester");
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("autocomplete")).clear();
        driver.findElement(By.id("autocomplete")).sendKeys("a");
        Thread.sleep(3000);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN, Keys.ENTER);
        driver.findElement(By.id("ContactNumber")).clear();
        driver.findElement(By.id("ContactNumber")).sendKeys("+631234567890");
        driver.findElement(By.id("Applicant_Skype")).clear();
        driver.findElement(By.id("Applicant_Skype")).sendKeys("http://skype.com/endoma");
        driver.findElement(By.id("Applicant_FacebookUrl")).clear();
        driver.findElement(By.id("Applicant_FacebookUrl")).sendKeys("http://facebook.com/facebookm");
        driver.findElement(By.id("Applicant_LinkedinUrl")).clear();
        driver.findElement(By.id("Applicant_LinkedinUrl")).sendKeys("http://linkedin.com/mylinkedinm");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(2000);
        String popupMessage = driver.findElement(By.xpath("//*[contains(text(), updateProfileSuccess)]")).getText();
        assertTrue(popupMessage.contains(updateProfileSuccess));
    }

    @Test
    public void testChangePasswordApplicant() throws Exception {
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Settings And Membership")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("OldPassword")).clear();
        driver.findElement(By.id("OldPassword")).sendKeys("password");
        driver.findElement(By.id("NewPassword")).clear();
        driver.findElement(By.id("NewPassword")).sendKeys("Passw0rd");
        driver.findElement(By.id("ConfirmPassword")).clear();
        driver.findElement(By.id("ConfirmPassword")).sendKeys("Passw0rd");
        driver.findElement(By.xpath("//input[@value='Change your password']")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(2000);
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Password")).clear();
        driver.findElement(By.id("Password")).sendKeys("Passw0rd");
        driver.findElement(By.id("Email")).clear();
        driver.findElement(By.id("Email")).sendKeys("qamendacc.two@gmail.com");
        driver.findElement(By.xpath("//input[@value='Sign In']")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Settings And Membership")).click();
        driver.findElement(By.id("OldPassword")).clear();
        driver.findElement(By.id("OldPassword")).sendKeys("Passw0rd");
        driver.findElement(By.id("NewPassword")).clear();
        driver.findElement(By.id("NewPassword")).sendKeys("password");
        driver.findElement(By.id("ConfirmPassword")).clear();
        driver.findElement(By.id("ConfirmPassword")).sendKeys("password");
        driver.findElement(By.xpath("//input[@value='Change your password']")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(2000);
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Password")).clear();
        driver.findElement(By.id("Password")).sendKeys("password");
        driver.findElement(By.id("Email")).clear();
        driver.findElement(By.id("Email")).sendKeys("qamendacc.two@gmail.com");
        driver.findElement(By.xpath("//input[@value='Sign In']")).click();
    }

    @Test
    public void testLogoutApplicant() throws Exception {
        driver.get(applicantDashboardURL);
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), url);
    }

    @Test
    public void checkHiredStatus() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        Thread.sleep(5000);
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(5000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Hired"));
    }

    @Test
    public void checkDeclinedStatus() throws Exception {
        driver.findElement(By.xpath("//div[@id='site-navbar-collapse']/ul[2]/li[3]/a/span")).click();
        Thread.sleep(5000);
        assertEquals(driver.getCurrentUrl(), candidatesPageURL);
        driver.findElement(By.xpath("//table[@id='candidates-table']/thead/tr/th[6]")).click();
        driver.findElement(By.cssSelector("th.sorting_asc")).click();
        if (page > 1) {
            for (int i = 1; i < page; i++) {
                driver.findElement(By.xpath("//li[@id='candidates-table_next']/a")).click();
            }
        }
        Thread.sleep(5000);
        String candidateStatus = driver.findElement(By.xpath("//table[@id='candidates-table']/tbody/tr[" + (index + 1) + "]")).getText();
        assertTrue(candidateStatus.contains("Declined"));
    }

    @Test
    public void checkApplicantDeclinedStatus() throws Exception {
        driver.get(appliedJobURL);
        Thread.sleep(2000);
        String text = driver.findElement(By.xpath("//*[contains(text(), declinedStatus)]")).getText();
        assertTrue(text.contains(declinedStatus));
    }

    @Test
    public void ApplyJob() throws Exception {
        validRecruiterLogin();
        createAJobDev();
        testOpenAJob();
        testLogout();
        validApplicantLogin();
        testApplyAJob();
        testLogoutApplicant();
    }

    @Test
    public void HireCandidate() throws Exception {
        validRecruiterLogin();
        testOfferJob();
        testLogout();
        validApplicantLogin();
        testAcceptOffer();
        testLogoutApplicant();
        validRecruiterLogin();
        checkHiredStatus();
        testLogout();
    }

    @Test
    public void CandidateDeclineOffer() throws Exception {
        validRecruiterLogin();
        testOfferJob();
        testLogout();
        validApplicantLogin();
        testApplicantDeclineOffer();
        testLogoutApplicant();
        validRecruiterLogin();
        checkDeclinedStatus();
        testLogout();
    }

    @Test
    public void DeclineAppliedApplicant() throws Exception {
        validRecruiterLogin();
        testDeclineApplication();
        testLogout();
        validApplicantLogin();
        checkApplicantDeclinedStatus();
        testLogoutApplicant();
    }

    @Test
    public void DeclineFirstContactApplicant() throws Exception {
        validRecruiterLogin();
        testDeclineFirstContact();
        testLogout();
        validApplicantLogin();
        checkApplicantDeclinedStatus();
        testLogoutApplicant();
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
