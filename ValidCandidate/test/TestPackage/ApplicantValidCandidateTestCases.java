package TestPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import static org.testng.Assert.*;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

/**
 *
 * @author Tirana
 */
public class ApplicantValidCandidateTestCases {

    private static WebDriver driver = null;

    //Test Data
    String webDriver = "webdriver.chrome.driver"; //"webdriver.gecko.driver"
    String webDriverURL = "C:\\webdrivers\\chromedriver.exe"; //"C:\\webdrivers\\geckodriver.exe"
    String uploadURL = "D:\\Documents\\Coding\\We! Technology\\Bug Report\\ValidCandidate\\Web\\icon.jpg";
    private boolean acceptNextAlert = true;
    String validApplicantUsername = "qamendacc.two@gmail.com";//"tirana.applicant3@gmail.com";
    String unconfirmedApplicantAccount = "tirana.applicant4@gmail.com";
    String newApplicantAccount = "tirana.test1@yahoo.com";
    String referenceEmail = "tirana.test2@gmail.com";
    String validPassword = "password";

    String url = "https://www.validcandidate.com/";
    String jobURL = "https://wetechnology2.validcandidate.com/jobs/6468/asp-net-frontend-developer";

    String hiredStatus = "Hired";
    String declinedStatus = "Declined Application";
    String cancelledStatus = "Application Cancelled";
    String updateProfileSuccess = "Successfully updated applicant information";

    //applicant in Prod
    String applicantDashboardURL = "https://www.validcandidate.com/applicant/dashboard";
    String applicantRegistrationURL = "https://www.validcandidate.com/account/applicant/registration";
    String appliedJobsURL = "https://www.validcandidate.com/Applicant/Applications";
    String referencesURL = "https://www.validcandidate.com/applicant/references";
    String applicantAssessmentURL = "https://www.validcandidate.com/assessmentcandidate";
    String applicantMessageURL = "https://www.validcandidate.com/messages";
    String EditApplicantProfileURL = "https://www.validcandidate.com/applicant/edit-profile";
    String ESApplicantDashboardURL = "https://www.validcandidate.com/es/applicant/dashboard";
    String ITApplicantDashboardURL = "https://www.validcandidate.com/it/applicant/dashboard";
    String UKApplicantDashboardURL = "https://www.validcandidate.com/uk/applicant/dashboard";
    String CSApplicantDashboardURL = "https://www.validcandidate.com/cs/applicant/dashboard";
    String SRApplicantDashboardURL = "https://www.validcandidate.com/sr/applicant/dashboard";
    String ENApplicantDashboardURL = "https://www.validcandidate.com/en/applicant/dashboard";

    //applicant in Dev
    //String appliedJobURL = "https://recruiter.wetechnology/Account/UnconfirmedAccount"; 
    //String applicantDashboardURL = "https://recruiter.wetechnology.cz/applicant/dashboard";
    public ApplicantValidCandidateTestCases() {
    }

    @BeforeSuite(alwaysRun = true)
    public void setupBeforeSuite() {
        System.setProperty(webDriver, webDriverURL);
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
    }

    @AfterSuite(alwaysRun = true)
    public void setupAfterSuite() throws InterruptedException {
        Thread.sleep(10000);
        driver.quit();
    }

    @Test(description = "Login using a valid applicant login")
    public void validApplicantLogin() throws Exception {
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(validApplicantUsername);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        assertEquals(driver.getCurrentUrl(), applicantDashboardURL);
    }

    @Test(description = "Sign Up as an applicant")
    public void signUpApplicant() throws Exception {
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As an Applicant")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Applicant Sign-up");
        assertTrue(driver.getCurrentUrl().contains(applicantRegistrationURL));

        driver.findElement(By.name("Email")).sendKeys(newApplicantAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.name("ConfirmPassword")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='checkbox'][id='is-term-condition-accepted']")).click();
        driver.findElement(By.cssSelector("input[type='submit'][value='Register']")).click();

        assertEquals(driver.getTitle(), "You've successfully registered - Valid Candidate");
    }

    @Test(description = "Login using LinkedIn")
    public void loginLinkedIn() throws Exception {
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test(description = "Sign Up as an Applicant using LinkedIn")
    public void signUpApplicantLinkedIn() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As an Applicant")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Applicant Sign-up");
        assertTrue(driver.getCurrentUrl().contains(applicantRegistrationURL));

        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test
    public void testApplyAJob() throws Exception {
        int jobCountBefore = Integer.parseInt(driver.findElement(By.cssSelector("div[id='open-job-count']")).getText());
        driver.get(jobURL);
        driver.findElement(By.linkText("Apply Now")).click();
        driver.findElement(By.id("ApplicationQuestionAnswer_0__Answer")).clear();
        driver.findElement(By.id("ApplicationQuestionAnswer_0__Answer")).sendKeys("Test");
        driver.findElement(By.id("ApplicationQuestionAnswer_1__Answer")).clear();
        driver.findElement(By.id("ApplicationQuestionAnswer_1__Answer")).sendKeys("Test");
        driver.findElement(By.linkText("Video Recording")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("div.modal-header > button.close")).click();
        Thread.sleep(2000);
        new Select(driver.findElement(By.id("Availability"))).selectByVisibleText("ASAP");
        driver.findElement(By.id("CoverLetter")).clear();
        driver.findElement(By.id("CoverLetter")).sendKeys("Test");
        driver.findElement(By.id("add-application-reference")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("ins.iCheck-helper")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(5000);
        int jobCountAfter = Integer.parseInt(driver.findElement(By.cssSelector("div[id='open-job-count']")).getText());
        assertEquals(jobCountBefore + 1, jobCountAfter);
    }

    @Test(description = "Opening Applied Jobs page")
    public void openAppliedJobs() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[2]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), appliedJobsURL);
    }

    @Test
    public void testAcceptOffer() throws Exception {
        driver.findElement(By.xpath("//a[@href='/Applicant/Applications?selectedApplicationStatus=offer']")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText("View Details")).click();
        driver.findElement(By.xpath("//li[4]/div")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        Thread.sleep(2000);
        String text = driver.findElement(By.xpath("//*[contains(text(), hiredStatus)]")).getText();
        assertTrue(text.contains(hiredStatus));
    }

    @Test
    public void testDeclineOffer() throws Exception {
        driver.findElement(By.xpath("//a[@href='/Applicant/Applications?selectedApplicationStatus=offer']")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText("View Details")).click();
        driver.findElement(By.xpath("//li[4]/div")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
        Thread.sleep(5000);
        String text = driver.findElement(By.xpath("//*[contains(text(), declinedStatus)]")).getText();
        assertTrue(text.contains(declinedStatus));
    }

    @Test
    public void testCancelApplication() throws Exception {
        driver.findElement(By.xpath("//a[@href='/Applicant/Applications?selectedApplicationStatus=applied']")).click();
        Thread.sleep(2000);
        driver.findElement(By.linkText("View Details")).click();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(5000);
        String text = driver.findElement(By.xpath("//*[contains(text(), cancelledStatus)]")).getText();
        assertTrue(text.contains(cancelledStatus));
    }

    @Test
    public void testRequestNewReference() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[3]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), referencesURL);
        driver.findElement(By.id("new-ref")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("ReferenceName")).clear();
        driver.findElement(By.id("ReferenceName")).sendKeys("Tirana Fatyanosa");
        driver.findElement(By.id("ReferenceEmail")).clear();
        driver.findElement(By.id("ReferenceEmail")).sendKeys(referenceEmail);
        driver.findElement(By.cssSelector("div.selected-flag")).click();
        driver.findElement(By.cssSelector("span.country-name")).click();
        driver.findElement(By.id("PhoneNumber")).clear();
        driver.findElement(By.id("PhoneNumber")).sendKeys("+11234567890");
        driver.findElement(By.id("Relationship")).clear();
        driver.findElement(By.id("Relationship")).sendKeys("Peer");
        driver.findElement(By.id("ContactAverage")).clear();
        driver.findElement(By.id("ContactAverage")).sendKeys("Sometimes");
        driver.findElement(By.id("JobPosition")).clear();
        driver.findElement(By.id("JobPosition")).sendKeys("QA Analyst");
        driver.findElement(By.id("Company")).clear();
        driver.findElement(By.id("Company")).sendKeys("We Technology");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//form[@id='frm-reference']/div/div[10]/button")).click();
        Thread.sleep(10000);
        String references = driver.findElement(By.xpath("//*[contains(text(), referenceEmail)]")).getText();
        assertTrue(references.contains(referenceEmail));
    }

    @Test
    public void testEditReference() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[3]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), referencesURL);
        int index = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div"));

        for (WebElement element : allElements) {
            if (element.getText().contains(referenceEmail)) {
                break;
            }
            index += 1;
        }

        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div[" + (index + 1) + "]/div[3]/button")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("ReferenceName")).clear();
        driver.findElement(By.id("ReferenceName")).sendKeys("Tirana Fatyanosa");
        driver.findElement(By.id("ReferenceEmail")).clear();
        driver.findElement(By.id("ReferenceEmail")).sendKeys(unconfirmedApplicantAccount);
        driver.findElement(By.cssSelector("div.selected-flag")).click();
        driver.findElement(By.cssSelector("span.country-name")).click();
        driver.findElement(By.id("PhoneNumber")).clear();
        driver.findElement(By.id("PhoneNumber")).sendKeys("+11234567890");
        driver.findElement(By.id("Relationship")).clear();
        driver.findElement(By.id("Relationship")).sendKeys("Peer");
        driver.findElement(By.id("ContactAverage")).clear();
        driver.findElement(By.id("ContactAverage")).sendKeys("Sometimes");
        driver.findElement(By.id("JobPosition")).clear();
        driver.findElement(By.id("JobPosition")).sendKeys("QA Analyst");
        driver.findElement(By.id("Company")).clear();
        driver.findElement(By.id("Company")).sendKeys("We Technology");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//form[@id='frm-reference']/div/div[10]/button")).click();
        Thread.sleep(10000);
        String references = driver.findElement(By.xpath("//*[contains(text(), unconfirmedAccount)]")).getText();
        assertTrue(references.contains(unconfirmedApplicantAccount));
    }

    @Test
    public void testDeleteReference() throws Exception {
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[3]/a/span")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), referencesURL);
        int index = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div"));

        for (WebElement element : allElements) {
            if (element.getText().contains(unconfirmedApplicantAccount)) {
                break;
            }
            index += 1;
        }

        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.xpath("//div[@id='page-content']/div[2]/div[3]/div/div[" + (index + 1) + "]/div[3]/button[2]")).click();
        Thread.sleep(5000);
        assertTrue(closeAlertAndGetItsText().matches("^Are you sure you want to delete this reference[\\s\\S]$"));
        Thread.sleep(5000);
        String references = driver.findElement(By.xpath("//*[contains(text(), unconfirmedAccount)]")).getText();
        assertFalse(references.contains(unconfirmedApplicantAccount));
    }

    @Test
    public void testGetAssessment() throws Exception {
        driver.findElement(By.cssSelector("a.toolbox-ico > img")).click();
        Thread.sleep(5000);
        assertEquals(driver.getCurrentUrl(), applicantAssessmentURL);
        String assessmentName = driver.findElement(By.xpath("//tbody[@id='partialDatalist']/tr/td")).getText();
        driver.findElement(By.xpath("//tbody[@id='partialDatalist']/tr/td")).click();
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("assessmentButtonBuyNow")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Successfully sent assessment request.";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        assertNotEquals(driver.findElement(By.xpath("//tbody[@id='partialDatalist']/tr/td")).getText(), assessmentName);

        driver.findElement(By.linkText("Assessment History")).click();
        Thread.sleep(5000);

        assertEquals(driver.findElement(By.xpath("//tbody[@id='modalAssessmentContainerHistory']/tr/td")).getText(), assessmentName.toUpperCase());
        assertEquals(driver.findElement(By.xpath("//tbody[@id='modalAssessmentContainerHistory']/tr/td[4]")).getText(), "PENDING");
    }

    @Test
    public void testSendMessageToRecruiter() throws Exception {
        String messageText = "Send a message.";
        driver.findElement(By.linkText("Details")).click();
        driver.findElement(By.linkText("Message")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("Message")).clear();
        driver.findElement(By.id("Message")).sendKeys(messageText);
        driver.findElement(By.cssSelector("button.browse.mf-browse-btn")).click();
        StringSelection ss = new StringSelection(uploadURL);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        Robot robot = new Robot();
        robot.delay(5000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(5000);
        robot.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(5000);
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        Thread.sleep(3000);
        String msg_in_alert = "Message sent successfully";
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));
        driver.findElement(By.xpath("//div[@id='main-dashboard-header']/nav/div[5]/a/img")).click();
        Thread.sleep(3000);
        assertEquals(driver.getCurrentUrl(), applicantMessageURL);
        int index = 0;
        List<WebElement> allElements = driver.findElements(By.xpath("//ul[@id='listOfDialogs']/li"));

        for (WebElement element : allElements) {
            driver.findElement(By.xpath("//ul[@id='listOfDialogs']/li")).click();
            String messages = driver.findElement(By.xpath("//*[contains(text(), messageText)]")).getText();
            if (messages.contains(messageText)) {
                assertTrue(messages.contains(messageText));
                break;
            }
        }
    }

    @Test
    public void testChangeLanguageApplicant() throws Exception {
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Español")).click();
        assertEquals(driver.getCurrentUrl(), ESApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Italiano")).click();
        assertEquals(driver.getCurrentUrl(), ITApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Українська")).click();
        assertEquals(driver.getCurrentUrl(), UKApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Český")).click();
        assertEquals(driver.getCurrentUrl(), CSApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("Српски")).click();
        assertEquals(driver.getCurrentUrl(), SRApplicantDashboardURL);
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.linkText("English")).click();
        assertEquals(driver.getCurrentUrl(), ENApplicantDashboardURL);
    }

    @Test
    public void testEditProfileApplicant() throws Exception {
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Edit Profile")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), EditApplicantProfileURL);
        driver.findElement(By.id("FirstName")).clear();
        driver.findElement(By.id("FirstName")).sendKeys("QAApplicant One");
        driver.findElement(By.id("LastName")).clear();
        driver.findElement(By.id("LastName")).sendKeys("Endoma Jr");
        driver.findElement(By.id("JobTitle")).clear();
        driver.findElement(By.id("JobTitle")).sendKeys("QA Tester");
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("autocomplete")).clear();
        driver.findElement(By.id("autocomplete")).sendKeys("a");
        Thread.sleep(3000);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
        driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN, Keys.ENTER);
        driver.findElement(By.id("ContactNumber")).clear();
        driver.findElement(By.id("ContactNumber")).sendKeys("+631234567890");
        driver.findElement(By.id("Applicant_Skype")).clear();
        driver.findElement(By.id("Applicant_Skype")).sendKeys("http://skype.com/endoma");
        driver.findElement(By.id("Applicant_FacebookUrl")).clear();
        driver.findElement(By.id("Applicant_FacebookUrl")).sendKeys("http://facebook.com/facebookm");
        driver.findElement(By.id("Applicant_LinkedinUrl")).clear();
        driver.findElement(By.id("Applicant_LinkedinUrl")).sendKeys("http://linkedin.com/mylinkedinm");
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-educ")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Course")).clear();
        driver.findElement(By.id("Course")).sendKeys("Master");
        driver.findElement(By.id("School")).clear();
        driver.findElement(By.id("School")).sendKeys("Harvard University");
        new Select(driver.findElement(By.id("YearStarted"))).selectByVisibleText("2013");
        new Select(driver.findElement(By.id("YearGraduated"))).selectByVisibleText("2015");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-exp")).click();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("div.col-md-12.col > #JobTitle")).clear();
        driver.findElement(By.cssSelector("div.col-md-12.col > #JobTitle")).sendKeys("Quality Assurance Specialist");
        driver.findElement(By.id("PrevCompany")).clear();
        driver.findElement(By.id("PrevCompany")).sendKeys("We Technology");
        new Select(driver.findElement(By.id("MonthFrom"))).selectByVisibleText("March");
        new Select(driver.findElement(By.id("YearFrom"))).selectByVisibleText("2015");
        new Select(driver.findElement(By.id("MonthTo"))).selectByVisibleText("February");
        new Select(driver.findElement(By.id("YearTo"))).selectByVisibleText("2016");
        driver.findElement(By.id("CurrentWork")).click();
        driver.findElement(By.id("JobDescription")).clear();
        driver.findElement(By.id("JobDescription")).sendKeys("Worked as a QA Specialist.");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-freelance")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("JobPosition")).clear();
        driver.findElement(By.id("JobPosition")).sendKeys("Quality Assurance Specialist");
        driver.findElement(By.id("ClientCompany")).clear();
        driver.findElement(By.id("ClientCompany")).sendKeys("We Technology");
        driver.findElement(By.id("PortfolioName")).clear();
        driver.findElement(By.id("PortfolioName")).sendKeys("LinkedIn");
        driver.findElement(By.id("PortfolioLink")).clear();
        driver.findElement(By.id("PortfolioLink")).sendKeys("http://linkedin.com");
        driver.findElement(By.id("FreelanceDescription")).clear();
        driver.findElement(By.id("FreelanceDescription")).sendKeys("Worked  as QA Specialist.");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-skill")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("SkillName")).clear();
        driver.findElement(By.id("SkillName")).sendKeys("QA");
        new Select(driver.findElement(By.id("SkillLevel"))).selectByVisibleText("Expert");
        driver.findElement(By.id("SkillDescription")).clear();
        driver.findElement(By.id("SkillDescription")).sendKeys("Worked for 5 years.");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-lang")).click();
        Thread.sleep(2000);
        new Select(driver.findElement(By.id("LanguageName"))).selectByVisibleText("Indonesian");
        driver.findElement(By.xpath("(//input[@id='Proficiency'])[4]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        driver.findElement(By.id("add-cert")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("CertificationName")).clear();
        driver.findElement(By.id("CertificationName")).sendKeys("Quality Assurance Certification");
        driver.findElement(By.id("CertificationFrom")).clear();
        driver.findElement(By.id("CertificationFrom")).sendKeys("We Technology");
        driver.findElement(By.id("CertificationDescription")).clear();
        driver.findElement(By.id("CertificationDescription")).sendKeys("QA Certification");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-course")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("CourseTitle")).clear();
        driver.findElement(By.id("CourseTitle")).sendKeys("QA Course");
        new Select(driver.findElement(By.id("TakenOn"))).selectByVisibleText("2015");
        driver.findElement(By.id("CourseDescription")).clear();
        driver.findElement(By.id("CourseDescription")).sendKeys("QA Course");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-award")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("AwardName")).clear();
        driver.findElement(By.id("AwardName")).sendKeys("QA Honor");
        new Select(driver.findElement(By.id("AwardedOn"))).selectByVisibleText("2016");
        driver.findElement(By.id("AwardDescription")).clear();
        driver.findElement(By.id("AwardDescription")).sendKeys("QA Honor");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-publication")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("PublicationTitle")).clear();
        driver.findElement(By.id("PublicationTitle")).sendKeys("QA Publication");
        new Select(driver.findElement(By.id("YearPublished"))).selectByVisibleText("2016");
        driver.findElement(By.id("PublicationDescription")).clear();
        driver.findElement(By.id("PublicationDescription")).sendKeys("QA Publication");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        jsx.executeScript("window.scrollBy(0,250)", "");
        driver.findElement(By.id("add-hobby")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("HobbyName")).clear();
        driver.findElement(By.id("HobbyName")).sendKeys("QA Hobby");
        driver.findElement(By.id("HobbyDescription")).clear();
        driver.findElement(By.id("HobbyDescription")).sendKeys("QA Hobby");
        driver.findElement(By.xpath("(//button[@type='button'])[22]")).click();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(2000);
        String popupMessage = driver.findElement(By.xpath("//*[contains(text(), updateProfileSuccess)]")).getText();
        assertTrue(popupMessage.contains(updateProfileSuccess));
    }

    @Test
    public void testChangePasswordApplicant() throws Exception {
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Settings And Membership")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("OldPassword")).clear();
        driver.findElement(By.id("OldPassword")).sendKeys("password");
        driver.findElement(By.id("NewPassword")).clear();
        driver.findElement(By.id("NewPassword")).sendKeys("Passw0rd");
        driver.findElement(By.id("ConfirmPassword")).clear();
        driver.findElement(By.id("ConfirmPassword")).sendKeys("Passw0rd");
        driver.findElement(By.xpath("//input[@value='Change your password']")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(2000);
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Password")).clear();
        driver.findElement(By.id("Password")).sendKeys("Passw0rd");
        driver.findElement(By.id("Email")).clear();
        driver.findElement(By.id("Email")).sendKeys("qamendacc.two@gmail.com");
        driver.findElement(By.xpath("//input[@value='Sign In']")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Settings And Membership")).click();
        driver.findElement(By.id("OldPassword")).clear();
        driver.findElement(By.id("OldPassword")).sendKeys("Passw0rd");
        driver.findElement(By.id("NewPassword")).clear();
        driver.findElement(By.id("NewPassword")).sendKeys("password");
        driver.findElement(By.id("ConfirmPassword")).clear();
        driver.findElement(By.id("ConfirmPassword")).sendKeys("password");
        driver.findElement(By.xpath("//input[@value='Change your password']")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(2000);
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("Password")).clear();
        driver.findElement(By.id("Password")).sendKeys("password");
        driver.findElement(By.id("Email")).clear();
        driver.findElement(By.id("Email")).sendKeys("qamendacc.two@gmail.com");
        driver.findElement(By.xpath("//input[@value='Sign In']")).click();
    }

    @Test
    public void testLogoutApplicant() throws Exception {
        driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
        driver.findElement(By.linkText("Log out")).click();
        Thread.sleep(2000);
        assertEquals(driver.getCurrentUrl(), url);
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
