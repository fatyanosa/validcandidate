/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.testng.Assert.*;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

/**
 *
 * @author Tirana
 */
public class ValidCandidateTestCases {

    private static WebDriver driver = null;

    //Test Data
    String webDriver = "webdriver.chrome.driver"; //"webdriver.gecko.driver"
    String webDriverURL = "C:\\webdrivers\\chromedriver.exe"; //"C:\\webdrivers\\geckodriver.exe"
    String url = "https://www.validcandidate.com/"; //"http://recruiter.wetechnology.cz"
    String validUsername = "qamendacc.one@gmail.com";
    String validPassword = "password";
    String unregisteredEmail = "testing@gmail.com";
    String invalidPassword = "testing123";
    String invalidEmail = "testing";
    String unconfirmedAccount = "tirana.fatyanosa.wetechnology@gmail.com";
    String unconfirmedAccountPassword = "testing123";
    String newAccount = "tirana.test1@gmail.com";
    
    String companyName = "Technology We";
    String subdomainAddress = "wetechnology";
    String websiteAddress = "http://wetechnology.cz/";
    String companyEmail = "tirana.fatyanosa@wetechnology.cz";

    public ValidCandidateTestCases() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @BeforeSuite(alwaysRun = true)
    public void setupBeforeSuite() {
        System.setProperty(webDriver, webDriverURL);
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
    }

    @AfterSuite(alwaysRun = true)
    public void setupAfterSuite() throws InterruptedException {
        Thread.sleep(10000);
        driver.quit();
    }

    @Test(description = "Launches the site")
    public void launchSite() throws Exception {
        assertEquals(driver.getTitle(), "Valid Candidate - Applicant Tracking System");
    }

    @Test(description = "Enters valid login data")
    public void validLogin() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(validUsername);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        assertEquals(driver.getCurrentUrl(), "https://www.validcandidate.com/dashboard");
    }

    @Test(description = "Enter unregistered email")
    public void unregisteredEmail() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(unregisteredEmail);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        String actual_msg = driver.findElement(By.xpath("//div[contains(@class,'validation-summary-errors text-danger')]")).getText();
        String expect = "Invalid login attempt.";
        assertEquals(actual_msg, expect);
    }

    @Test(description = "Enter invalid password")
    public void invalidPassword() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(validUsername);
        driver.findElement(By.name("Password")).sendKeys(invalidPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        String actual_msg = driver.findElement(By.xpath("//div[contains(@class,'validation-summary-errors text-danger')]")).getText();
        String expect = "Invalid login attempt.";
        assertEquals(actual_msg, expect);
    }

    @Test(description = "Enter invalid email")
    public void invalidEmail() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(invalidEmail);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        String actual_msg = driver.findElement(By.xpath("//span[contains(@class,'text-danger field-validation-error')]")).getText();
        String expect = "The Email field is not a valid e-mail address.";
        assertEquals(actual_msg, expect);
    }

    @Test(description = "Enter an uncorfirmed account")
    public void unconfirmedAccount() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(unconfirmedAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();

        assertEquals(driver.getTitle(), "Resend Email Confirmation - Valid Candidate");
        assertTrue(driver.getCurrentUrl().contains("https://www.validcandidate.com/Account/UnconfirmedAccount"));
    }

    @Test(description = "Resend confirmation email")
    public void resendConfirmationEmail() throws Exception {
        driver.findElement(By.xpath("//button[contains(@class,'btn btn-ats ats-primary')]")).click();
        assertEquals(driver.getTitle(), "You've successfully registered - Valid Candidate");
    }

    @Test(description = "Confirmation email sent to the account's email")
    public void confirmationEmailSent() throws Exception {
        //Open gmail
        driver.get("http://www.gmail.com");

        // Enter userd id
        WebElement element = driver.findElement(By.id("identifierId"));
        element.sendKeys(unconfirmedAccount);

        //wait 5 secs for userid to be entered
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //driver.findElement(By.cssSelector("input[value='Next']")).click();
        //driver.findElement(By.xpath("//span[contains(@value,'Next')]")).click();
        driver.findElement(By.xpath("//span[contains(@class,'RveJvd snByac') and contains(text(), 'Next')]")).click();

        //Enter Password
        WebElement element1 = driver.findElement(By.name("password"));
        element1.sendKeys(unconfirmedAccountPassword);

        //wait 5 secs for userid to be entered
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.xpath("//div[contains(@id,'passwordNext')]")).click();
        //driver.findElement(By.id("passwordNext")).click();

        driver.findElement(By.xpath("//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();

        List<WebElement> unreademeil = driver.findElements(By.xpath("//*[@class='zF']"));
    }

    @Test(description = "Login using LinkedIn")
    public void loginLinkedIn() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test(description = "Sign Up as a Recruiter")
    public void signUpRecruiter() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As a Recruiter")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Recruiter Sign-up");
        assertTrue(driver.getCurrentUrl().contains("https://www.validcandidate.com/account/recruiter/registration"));

        driver.findElement(By.name("Email")).sendKeys(unconfirmedAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.name("ConfirmPassword")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='checkbox'][id='is-term-condition-accepted']")).click();
        driver.findElement(By.cssSelector("input[type='submit'][value='Register']")).click();

        assertEquals(driver.getTitle(), "You've successfully registered - Valid Candidate");
    }

    @Test(description = "Sign Up as a Recruiter using LinkedIn")
    public void signUpRecruiterLinkedIn() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As a Recruiter")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Recruiter Sign-up");
        assertTrue(driver.getCurrentUrl().contains("https://www.validcandidate.com/account/recruiter/registration"));

        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test(description = "Sign Up as an Applicant")
    public void signUpApplicant() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As an Applicant")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Applicant Sign-up");
        assertTrue(driver.getCurrentUrl().contains("https://www.validcandidate.com/account/applicant/registration"));

        driver.findElement(By.name("Email")).sendKeys(unconfirmedAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.name("ConfirmPassword")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='checkbox'][id='is-term-condition-accepted']")).click();
        driver.findElement(By.cssSelector("input[type='submit'][value='Register']")).click();

        assertEquals(driver.getTitle(), "You've successfully registered - Valid Candidate");
    }

    @Test(description = "Sign Up as an Applicant using LinkedIn")
    public void signUpApplicantLinkedIn() throws Exception {
        driver.get(url);
        driver.findElement(By.xpath("//button[contains(@class,'dropdown-toggle profile-top account-div') and contains(text(), 'Sign up')]")).click();
        driver.findElement(By.linkText("As an Applicant")).click();
        assertEquals(driver.getTitle(), "Valid Candidate Software - Applicant Sign-up");
        assertTrue(driver.getCurrentUrl().contains("https://www.validcandidate.com/account/applicant/registration"));

        driver.findElement(By.id("LinkedIn")).click();
        String msg_in_alert = "invalid redirect_uri. This value must match a URL registered with the API Key.";

        assertNotEquals(driver.findElement(By.cssSelector("div[class='alert error']")).getText(), (msg_in_alert));
    }

    @Test(description = "Login using confirmed account (new account)")
    public void loginNewAccount() throws Exception {
        driver.get(url);
        driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.name("Email")).sendKeys(newAccount);
        driver.findElement(By.name("Password")).sendKeys(validPassword);
        driver.findElement(By.cssSelector("input[type='submit'][value='Sign In']")).click();
        assertEquals(driver.getCurrentUrl(), "https://www.validcandidate.com/dashboard/personal-profile");
    }

    @Test(description = "Create a new company")
    public void createNewCompany() throws Exception {
        driver.findElement(By.cssSelector("span[class='avatar avatar-online']")).click();
        driver.findElement(By.linkText("Create a new company")).click();
        assertEquals(driver.getCurrentUrl(), "https://www.validcandidate.com/dashboard/company/new");

        driver.findElement(By.id("Company_CompanyName")).sendKeys(companyName);
        driver.findElement(By.id("Company_Subdomain")).sendKeys(subdomainAddress);
        driver.findElement(By.id("Company_WebsiteAddress")).sendKeys(websiteAddress);
        driver.findElement(By.id("Company_CompanyEmail")).sendKeys(companyEmail);
        driver.findElement(By.cssSelector("input[type='submit'][value='Save']")).click();
        String msg_in_alert ="Successfully created new company.";        
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));        
    }
    
    @Test(description = "Create an existing company")
    public void createExistCompany() throws Exception {
        driver.findElement(By.cssSelector("span[class='avatar avatar-online']")).click();
        driver.findElement(By.linkText("Create a new company")).click();
        assertEquals(driver.getCurrentUrl(), "https://www.validcandidate.com/dashboard/company/new");

        driver.findElement(By.id("Company_CompanyName")).sendKeys(companyName);
        driver.findElement(By.id("Company_Subdomain")).sendKeys(subdomainAddress);
        driver.findElement(By.id("Company_WebsiteAddress")).sendKeys(websiteAddress);
        driver.findElement(By.id("Company_CompanyEmail")).sendKeys(companyEmail);
        driver.findElement(By.cssSelector("input[type='submit'][value='Save']")).click();
        String msg_in_alert ="You already have a company with same name";        
        assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));        
    }
}
