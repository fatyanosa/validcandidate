package TestPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URISyntaxException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

/**
 *
 * @author Tirana
 */
public class DownloadItTestCases {

    private static WebDriver driver = null;

    //Test Data
    private String webDriver = "webdriver.gecko.driver"; //"webdriver.chrome.driver"; 
    private String webDriverURL = "C:\\webdrivers\\geckodriver.exe"; //"C:\\webdrivers\\chromedriver.exe"; 
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String dashboardURL = "http://anususka.com/Admin/Dashboard";
    private String uploadURL = "D:\\Documents\\Coding\\We! Technology\\Bug Report\\Download.it\\Web\\FS#1194.mp4";
    private String fileName = "FS#1194.mp4";

    @BeforeSuite(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty(webDriver, webDriverURL);
        driver = new FirefoxDriver();
        baseUrl = "http://anususka.com/";
    }

    @Test
    public void testLogin() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("LoginLink")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("7421911@aruba.it");
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("F9QfVG51Mg");
        driver.findElement(By.id("loginSignInButton")).click();
        Thread.sleep(3000);
        assertTrue(driver.getCurrentUrl().contains(dashboardURL));
    }

    @Test(enabled = false)
    public void testUpload() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        Thread.sleep(30000);
        //int totalFilesCountBefore = Integer.parseInt(driver.findElement(By.xpath("//span[contains(@id,'TotalFilesCount')]")).getText());
        String usedSpaceBefore = driver.findElement(By.xpath("//div[contains(@id, 'tour-bars')]/h5")).getText();
        driver.findElement(By.id("exampleUploadForm")).click();
        //Thread.sleep(10000);
        StringSelection ss = new StringSelection(uploadURL);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        Robot robot = new Robot();
        robot.delay(10000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(10000);
        robot.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(100000);

        //driver.get(baseUrl + "/Admin/Dashboard");
        //Thread.sleep(30000);
        //int totalFilesCountAfter = Integer.parseInt(driver.findElement(By.xpath("//span[contains(@id,'TotalFilesCount')]")).getText());
        // assertEquals(totalFilesCountAfter, totalFilesCountBefore + 1);
        String usedSpaceAfter = driver.findElement(By.xpath("//div[contains(@id, 'tour-bars')]/h5")).getText();
        assertNotEquals(usedSpaceBefore, usedSpaceAfter);

        String uncategorizedFiles = driver.findElement(By.xpath("//*[contains(text(), fileName)]")).getText();
        assertTrue(uncategorizedFiles.contains(fileName));
    }

    @Test(enabled = false)
    public void testDeleteFile() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        Thread.sleep(30000);
        String usedSpaceBefore = driver.findElement(By.xpath("//div[contains(@id, 'tour-bars')]/h5")).getText();
        //click delete file
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();
        Thread.sleep(10000);
        driver.findElement(By.linkText("Delete")).click();

        //click OK to delete file
        driver.findElement(By.xpath("//div[4]/div[2]/button")).click();
        //String msg_in_alert = "You already have a company with same name";
        //assertEquals(driver.findElement(By.cssSelector("div[class='toast-message']")).getText(), (msg_in_alert));

        Thread.sleep(5000);
        String uncategorizedFiles = driver.findElement(By.xpath("//*[contains(text(), fileName)]")).getText();
        assertFalse(uncategorizedFiles.contains(fileName));

        String usedSpaceAfter = driver.findElement(By.xpath("//div[contains(@id, 'tour-bars')]/h5")).getText();
        assertNotEquals(usedSpaceBefore, usedSpaceAfter);
    }

    @Test
    public void testAddFileToCategory() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();
        driver.findElement(By.linkText("Details")).click();
        driver.findElement(By.id("settings-modal-btn")).click();
        new Select(driver.findElement(By.id("file-category"))).selectByVisibleText("Franco New Category - Pics");
        driver.findElement(By.id("save-file-settings-btn")).click();
        driver.findElement(By.xpath("//tbody[@id='files-table-data']/tr/td[7]/div/i[2]")).click();
    }

    @Test
    public void testCommentFile() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();
        driver.findElement(By.linkText("Details")).click();
        driver.findElement(By.linkText("File Comments")).click();
        driver.findElement(By.id("txtAddComment")).clear();
        driver.findElement(By.id("txtAddComment")).sendKeys("Test");
        driver.findElement(By.xpath("(//button[@type='button'])[65]")).click();
        driver.findElement(By.cssSelector("a.edit-file-comment")).click();
        driver.findElement(By.id("txtAddComment")).clear();
        driver.findElement(By.id("txtAddComment")).sendKeys("Test 123");
        driver.findElement(By.xpath("(//button[@type='button'])[65]")).click();
        driver.findElement(By.linkText("Remove")).click();
    }

    @Test
    public void testUploadDownloadNewVersion() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();
        driver.findElement(By.linkText("Details")).click();
        driver.findElement(By.linkText("File Versions")).click();
        driver.findElement(By.id("add-file-version-btn")).click();
        driver.findElement(By.cssSelector("#frmAddFileVersion > input[name=\"file\"]")).click();
        driver.findElement(By.cssSelector("#frmAddFileVersion > input[name=\"file\"]")).clear();
        driver.findElement(By.cssSelector("#frmAddFileVersion > input[name=\"file\"]")).sendKeys("D:\\Documents\\Coding\\We! Technology\\Bug Report\\Download.it\\Web\\FS#1194.mp4");
        driver.findElement(By.xpath("//tbody[@id='files-table-data']/tr/td[7]/div/i[2]")).click();
        driver.findElement(By.linkText("File Versions")).click();
        driver.findElement(By.xpath("(//a[contains(text(),'Download')])[4]")).click();
    }

    @Test
    public void testShareUnshareSubuser() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("//tbody[@id='files-table-data']/tr/td[6]/div/div/div/div/i")).click();
        driver.findElement(By.cssSelector("li.addMember-list-item.addMember-selected")).click();
        driver.findElement(By.cssSelector("i.md-minus-circle")).click();
    }

    @Test
    public void testAddUserFromDashboard() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[6]")).click();
        driver.findElement(By.id("SubuserFirstName")).clear();
        driver.findElement(By.id("SubuserFirstName")).sendKeys("Tirana");
        driver.findElement(By.id("SubuserLastName")).clear();
        driver.findElement(By.id("SubuserLastName")).sendKeys("Fatyanosa");
        driver.findElement(By.id("SubuserEmail")).clear();
        driver.findElement(By.id("SubuserEmail")).sendKeys("tirana.test2@gmail.com");
        driver.findElement(By.id("SubuserCountry")).clear();
        driver.findElement(By.id("SubuserCountry")).sendKeys("India");
        driver.findElement(By.id("SubuserCity")).clear();
        driver.findElement(By.id("SubuserCity")).sendKeys("New Delhi");
        driver.findElement(By.id("SubuserStreet")).clear();
        driver.findElement(By.id("SubuserStreet")).sendKeys("123");
        driver.findElement(By.id("SubuserPassword")).clear();
        driver.findElement(By.id("SubuserPassword")).sendKeys("Passw0rd");
        driver.findElement(By.id("SubuserConfirmPassword")).clear();
        driver.findElement(By.id("SubuserConfirmPassword")).sendKeys("Passw0rd");
        driver.findElement(By.id("CreateSubuserSubmitButton")).click();
    }

    @Test
    public void testUploadFilePlusButton() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[39]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[40]")).click();
        // ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
    }

    @Test
    public void testCreateCategoryPlusButton() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[40]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[42]")).click();
        driver.findElement(By.id("Name")).clear();
        driver.findElement(By.id("Name")).sendKeys("New Category");
        driver.findElement(By.id("Desc")).clear();
        driver.findElement(By.id("Desc")).sendKeys("New Category");
        new Select(driver.findElement(By.name("PeriodType"))).selectByVisibleText("OneWeek");
        driver.findElement(By.id("create-category-btn")).click();
    }

    @Test
    public void testEditCategoryFromDashboard() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[38]")).click();
        driver.findElement(By.linkText("Edit")).click();
        driver.findElement(By.xpath("(//input[@id='Name'])[2]")).clear();
        driver.findElement(By.xpath("(//input[@id='Name'])[2]")).sendKeys("Newest Category");
        driver.findElement(By.xpath("(//textarea[@id='Desc'])[2]")).clear();
        driver.findElement(By.xpath("(//textarea[@id='Desc'])[2]")).sendKeys("Newest Category");
        new Select(driver.findElement(By.xpath("(//select[@name='PeriodType'])[2]"))).selectByVisibleText("TwoWeeks");
        driver.findElement(By.id("edit-category-btn")).click();
    }

    @Test
    public void testShareCategoryViaEmailFromDashboard() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[38]")).click();
        driver.findElement(By.linkText("Share via email")).click();
        new Select(driver.findElement(By.name("durationType"))).selectByVisibleText("2 Weeks");
        driver.findElement(By.id("emailTo")).clear();
        driver.findElement(By.id("emailTo")).sendKeys("tirana.fatyanosa.wetechnology@gmail.com");
        driver.findElement(By.id("emailFrom")).clear();
        driver.findElement(By.id("emailFrom")).sendKeys("tirana.test1@gmail.com");
        driver.findElement(By.id("messageText")).clear();
        driver.findElement(By.id("messageText")).sendKeys("Test category via email");
        driver.findElement(By.id("share-category-via-email-btn")).click();
    }

    @Test
    public void testDeleteCategoryFromDashboard() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard");
        driver.findElement(By.xpath("(//button[@type='button'])[38]")).click();
        driver.findElement(By.xpath("(//a[contains(text(),'Delete')])[32]")).click();
        driver.findElement(By.xpath("//div[4]/div[2]/button")).click();
    }

    @Test
    public void testFilterFiles() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard/Files");
        driver.findElement(By.linkText("All")).click();
        driver.findElement(By.linkText("Images")).click();
        driver.findElement(By.linkText("Audio")).click();
        driver.findElement(By.linkText("Video")).click();
        driver.findElement(By.linkText("Documents")).click();
        driver.findElement(By.linkText("Notes")).click();
    }

    @Test
    public void testViewCategoryFiles() throws Exception {
        driver.get(baseUrl + "/Admin/Dashboard/Files");
        driver.findElement(By.linkText("Franco New Category - Pics")).click();
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();

        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}
